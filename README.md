# Master Thesis, University of Copenhagen
## R(K*) Machine Learning Analysis Code
### Created by: _Daniel Hans Munk_, 2023
___
R(K*) group at CERN:
https://gitlab.cern.ch/RKstar

___
## Setup

### Pip enviroment
```
pip install -r requirements.txt
```
### Conda enviroment
```
conda env create -n ENVNAME --file RKStar_Analysis_enviroment.yml.yml
```
