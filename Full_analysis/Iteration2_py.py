# %%
print('Kernel Initialized')
#pid 115079

# %%
# Imports
import sys
sys.path.append("..")
import pandas as pd
import numpy as np
from utils import Utils
import plot_utils as plotting
import toml
from sklearn import preprocessing, model_selection, metrics
import seaborn as sns
import random

import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
plt.style.use("seaborn-v0_8-whitegrid")
#plt.style.use("seaborn-v0_8")
#import mplhep as hep
#hep.style.use(hep.style.ATLAS)
from scipy.special import logit, expit
import os

import verstack
import optuna
import scipy
import lightgbm

random.seed(42)

# %%
config = toml.load('config.toml')
Storage = Utils.HDF5_IO(config['PATHS']['Storage'])

# %%
def Data_loader():
    print('Loading MC signal..')
    B_masses = ['B_mass', 'B_mass_true','B_Bbar_mass_closer_PDG','Bbar_mass','BeeKst_B_mass','BeeKst_Bbar_mass','B_mass_Kstar_mass_closer']

    SIG_MC=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['USR'], columns=None, use_threads=True, storage_options=None)
    SIG_MC = SIG_MC.query(Utils.selection_cuts()['usr_lowq2'])
    SIG_MC.drop(SIG_MC[~((SIG_MC.BeeKst_isTrueNonresBd==1) | (SIG_MC.BeeKst_isTrueNonresBdbar==1))].index,inplace=True)
    SIG_MC.drop(SIG_MC[(SIG_MC.BeeKst_isTrueNonresBd==1)].index[config['ML_config']['N_Signal']['Bd']:],inplace=True)
    SIG_MC.drop(SIG_MC[(SIG_MC.BeeKst_isTrueNonresBdbar==1)].index[config['ML_config']['N_Signal']['BdBar']:],inplace=True)


    label = (SIG_MC.info_sample == 0)*0
    for i in config['labels Sig_VS_Bkg']:
        label += (SIG_MC.info_sample == str(i))*config['labels Sig_VS_Bkg'][str(i)]

    #SIG_MC=SIG_MC.drop(columns=info_features)
    #SIG_MC=SIG_MC.drop(columns=B_masses)
    print('Colomns of only on value is removed..')
    SIG_MC=SIG_MC.drop(SIG_MC.std()[(SIG_MC.std() == 0)].index, axis=1)
    print('Dublecates is removed..')
    SIG_MC=SIG_MC.loc[:,~SIG_MC.T.duplicated(keep='last')]
    SIG_MC['label'] = label
    SIG_MC['label'].astype(int)


    print('Loading background sidebands..')
    BKG_SB=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['SB'], columns=None, use_threads=True, storage_options=None)
    BKG_SB = BKG_SB.query(Utils.selection_cuts()['nn1trb_lowq2'])
    BKG_SB.drop(BKG_SB.index[config['ML_config']['N_background']:],inplace=True)
    label = (BKG_SB.info_sample == 0)*0
    for i in config['labels Sig_VS_Bkg']:
        label += (BKG_SB.info_sample == str(i))*config['labels Sig_VS_Bkg'][str(i)]
    BKG_SB['label'] = label
    BKG_SB['label'].astype(int)

    print('Loading background signal region..')
    BKG_SR=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['SR'], columns=None, use_threads=True, storage_options=None)
    BKG_SR = BKG_SR.query(Utils.selection_cuts()['nn2trb_lowq2'])
    BKG_SR.drop(BKG_SR.index[config['ML_config']['N_background']:],inplace=True)
    label = (BKG_SR.info_sample == 0)*0
    for i in config['labels Sig_VS_Bkg']:
        label += (BKG_SR.info_sample == str(i))*config['labels Sig_VS_Bkg'][str(i)]
    BKG_SR['label'] = label
    BKG_SR['label'].astype(int)

    print('Loading MC background..')
    BKG_MC=pd.read_feather(path=config['train_files']['Bkg_MC'], columns=None, use_threads=True, storage_options=None)
    BKG_MC = BKG_MC.query(Utils.selection_cuts()['main'])
    BKG_MC.info_sample = 'BKG_MC'
    label = (BKG_MC.info_sample == 0)*0
    for i in config['labels Sig_VS_Bkg']:
        label += (BKG_MC.info_sample == str(i))*config['labels Sig_VS_Bkg'][str(i)]
    BKG_MC['label'] = label
    BKG_MC['label'].astype(int)


    # Generate tests..
    print('Loading tests..')
    BKG_MC_test=pd.read_feather(path=config['train_files']['Bkg_MC'], columns=None, use_threads=True, storage_options=None)
    BKG_MC_test = BKG_MC_test.query(Utils.selection_cuts()['main'])
    BKG_MC_test.info_sample = 'BKG_MC_test'
    BKG_MC_test['label'] = 1


    BKG_SB_test=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['SB'], columns=None, use_threads=True, storage_options=None)
    BKG_SB_test = BKG_SB_test.query(Utils.selection_cuts()['nn1trb_lowq2'])
    BKG_SB_test.drop(BKG_SB_test.index[len(BKG_MC_test):],inplace=True)
    BKG_SB_test['label'] = 0

    BKG_SR_test=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['SR'], columns=None, use_threads=True, storage_options=None)
    BKG_SR_test = BKG_SR_test.query(Utils.selection_cuts()['nn2trb_lowq2'])
    BKG_SR_test.drop(BKG_SR_test.index[len(BKG_MC_test):],inplace=True)
    BKG_SR_test['label'] = 0


    return SIG_MC, BKG_SB, BKG_SR, BKG_MC, {'SB':BKG_SB_test, 'SR': BKG_SR_test, 'MC': BKG_MC_test }

SIG_MC, BKG_SB, BKG_SR, BKG_MC, MC_tests = Data_loader()


# %%
info_features = config['features']['info_features'] + ['track_multiplicity_after_cuts', 'electron_multiplicity_after_cuts', 'entry', 'subentry','mc_channel_number','event_number']
B_masses = ['B_mass', 'B_mass_true','B_Bbar_mass_closer_PDG','Bbar_mass','BeeKst_B_mass','BeeKst_Bbar_mass','B_mass_Kstar_mass_closer']
non_contributing_features = []
bad_features = ['diElectron_mass', 'BeeKst_PV_minZ0_y', 'BeeKst_PV_minA0_y', 'BeeKst_PV_minZ0_x']
feature_remove = ['label'] + info_features + B_masses + non_contributing_features + bad_features

#features = [i for i in list(SIG_MC.columns) if i not in feature_remove]


features = ['trackMinus_pT', 'BeeKst_Bbar_tau_invM_PVMinA0', 'electron_LHTightIsEMValue', 'dR_electron_trackPlus', 'track1_p', 'Lxy_significance_over_B_chi2', 'trackPlus_chi2_over_nDoF', 'trackMinus_p', 'diMeson_Kpi_piK_mass_ratio', 'mass_electron_trackMinus_kaon', 'track1_iso_c40', 'mass_electron_trackPlus_kaon', 'BeeKst_PV_minZ0_z_err', 'diMeson_Kpi_piK_mass_pT_ratio', 'B_chi2', 'dPhi_diElectron_trackMinus', 'mass_electron_trackPlus_pion', 'diMeson_piK_mass_pT', 'dR_trackPlus_trackMinus', 'electron1_pT', 'BeeKst_diElectron_pT', 'diElectron_pvalue', 'energy_trackPlus_pion', 'diMeson_angle_beta', 'B_chi2_over_nDoF', 'BeeKst_meson0_qOverP', 'dR_diElectron_trackPlus', 'BeeKst_z0_maxSumPt2', 'B_pvalue', 'positron_pT', 'diMeson_Kpi_piK_mass_diff', 'diElectron_iso_m25', 'diMeson_KK_mass', 'positron_ECIDSResult', 'diElectron_chi2_over_nDoF', 'BeeKst_diElectron_vtx_y_err', 'tracks_dEdx_diff', 'dPhi_positron_electron', 'BeeKst_meson1_d0', 'BeeKst_a0xy_maxSumPt2', 'BeeKst_diMeson_vtx_y_err', 'positron_p', 'tracks_dEdx_sum', 'BeeKst_x_err', 'a0_significance', 'mass_positron_trackMinus_pion', 'BeeKst_PV_minA0_y_err', 'dPhi_electron_trackMinus', 'electron1_p', 'diElectron_iso_m15', 'BeeKst_diMeson_vtx_z_err', 'diMeson_Kpi_mass', 'mass_electron_trackMinus_pion', 'electron0_pT', 'BeeKst_diElectron_vtx_x_err', 'BeeKst_meson0_passLoosePrimary', 'd_B_diMeson_significance', 'positron_nDoF', 'dR_positron_trackPlus', 'B_pT', 'BeeKst_Lxy_minA0_err', 'diElectron_iso_m10', 'diMeson_Kpi_piK_mass_pT_diff', 'BeeKst_a0_maxSumPt2_err', 'electron0_p', 'electron_chi2_over_nDoF', 'electron_p', 'dR_positron_Kstar', 'diMeson_piK_mass', 'BeeKst_z_err', 'track1_pT', 'BeeKst_diMeson_pT', 'trackPlus_pvalue', 'track0_dEdx', 'BeeKst_PV_minA0_x', 'diElectron_iso_m30', 'BeeKst_meson1_qOverP', 'diElectron_iso_m00', 'positron_LHMediumIsEMValue', 'dR_electron_Kstar', 'electron0_iso_c40', 'dR_electron_trackMinus', 'diMeson_Kpi_mass_pT', 'positron_chi2', 'cos_Theta_pseudo_err', 'dR_positron_electron', 'BeeKst_y', 'trackPlus_p', 'positron_ptvarcone40_TightTTVALooseCone_pt1000', 'electron_LHMediumIsEMValue', 'electron_ptvarcone40_TightTTVALooseCone_pt1000', 'BeeKst_B_mass_err', 'diElectron_pT', 'track1_dEdx', 'BeeKst_Bbar_tau_invM_PVMaxSumPt2_err', 'BeeKst_PV_minZ0_x_err', 'mass_pionKaon_kaonPion_significance', 'mass_positron_trackMinus_kaon', 'mass_pionKaon_kaonPion_ratio', 'mass_positron_trackPlus_kaon', 'd_B_diElectron_significance', 'electron1_iso_c15', 'dR_positron_trackMinus', 'd_diMeson_diElectron_significance', 'diElectron_p', 'positron_iso_c15', 'angle_pp_plane_ee_plane', 'B_p', 'angle_ee_plane_mm_plane', 'BeeKst_PV_minZ0_y_err', 'trackPlus_nDoF', 'BeeKst_diElectron_vtx_z_err', 'trackMinus_chi2_over_nDoF', 'diMeson_angle_beta_sym', 'angle_pp_plane_mm_plane', 'trackPlus_dEdx', 'diElectron_angle_alpha_sym', 'trackMinus_pvalue', 'trackMinus_chi2', 'diMeson_angle_alpha_sym', 'BeeKst_PV_minA0_x_err', 'trackPlus_pT', 'diElectron_angle_alpha', 'BeeKst_PV_minA0_z_err', 'kaon_pT', 'BeeKst_meson0_d0', 'diElectron_angle_beta', 'BeeKst_B_tau_invM_PVMaxSumPt2_err', 'dPhi_trackPlus_trackMinus', 'diMeson_pipi_mass', 'tracks_dEdx_ratio', 'trackMinus_nDoF', 'BeeKst_diMeson_vtx_x_err', 'mass_positron_trackPlus_pion', 'electron_iso_c35', 'track0_iso_c40', 'diMeson_Kpi_piK_mass_avg', 'BeeKst_meson1_passTightPrimary', 'electron_pT', 'BeeKst_diMeson_pz', 'trackPlus_iso_c35', 'Lxy_significance', 'trackMinus_dEdx', 'BeeKst_electron1_qOverP', 'diMeson_Kpi_piK_mass_closer_PDG', 'diElectron_iso_m20', 'dR_diElectron_Kstar', 'diElectron_iso_m05', 'dR_diMeson_B_diElectron', 'pion_pT', 'angle_vtx_plane_ee_plane', 'track0_pT', 'dPhi_diElectron_trackPlus', 'electron0_pz', 'BeeKst_Bbar_mass_err', 'BeeKst_a0xy_maxSumPt2_err', 'BeeKst_electron0_d0', 'BeeKst_a0xy_minA0']

# %%


# %%
def train_test_split_stratified(X):
    random.seed(42)
    ratios = {
        'train': 0.7,
        'valid': 0.0,
        'test': 0.30}
    np.random.seed(42)
    X_train, X_rem, y_train, y_rem = model_selection.train_test_split(X, X.label, train_size=ratios['train'],stratify=X.label, shuffle=True,random_state=42)
    if ratios['valid'] == 0:
        X_test = X_rem
        y_test = y_rem
        return {'X_train': X_train, 'y_train': y_train, 'X_test': X_test, 'y_test': y_test}
    else:
        X_valid, X_test, y_valid, y_test = model_selection.train_test_split(X_rem, y_rem, test_size=(ratios['test']/(1-ratios['train'])),stratify=y_rem,shuffle=True,random_state=42)
        return {'X_train': X_train, 'y_train': y_train, 'X_valid': X_valid, 'y_valid': y_valid, 'X_test': X_test, 'y_test': y_test}


# %%
# Split and Scale
Sig_vs_BKG_SB = pd.concat([SIG_MC,BKG_SB,BKG_MC.query(Utils.selection_cuts()['MC2'])])
Sig_vs_BKG_SB_MC_test = pd.concat([MC_tests['SB'],MC_tests['MC']])

Sig_vs_BKG_SB_splits = train_test_split_stratified(Sig_vs_BKG_SB)
Utils.fit_scaler_to_train(Storage,Sig_vs_BKG_SB_splits['X_train'][features],root_tag='BDT_SB_iter2')
Sig_vs_BKG_SB_splits['X_train_scaled'] = Utils.pd_scale(Storage,Sig_vs_BKG_SB_splits['X_train'][features], root_tag= 'BDT_SB_iter2', target_tag = 'X_train')
Sig_vs_BKG_SB_splits['X_test_scaled'] = Utils.pd_scale(Storage,Sig_vs_BKG_SB_splits['X_test'][features], root_tag= 'BDT_SB_iter2', target_tag = 'X_test')


# %%

def Run_tuner(X_,y_,Storage=Storage,root_tag='BDT_SB'):

    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"

    np.random.seed(42)
    #print(f"Training LGBM with {Storage.read_pd(key=f"{root_tag}/X_train").shape[1]} features")
    tuner = verstack.LGBMTuner(metric = 'log_loss',trials=100, visualization = False, seed = 42,verbosity=1)
    
    tuner.fit(X=X_,y=y_.astype(int).squeeze())

    Storage.write_pd(key=f"{root_tag}/tuner", df = tuner.study.trials_dataframe(attrs=('number', 'value', 'params', 'state','intermediate_values'),multi_index=True),overwrite=True)
    Storage.write_pd(key=f"{root_tag}/tuner_param_imp", df = pd.DataFrame([dict(optuna.importance.get_param_importances(tuner.study).items())]).T.rename(columns={0:'value'}),overwrite=True)
    Storage.save_lgbm(key=f"{root_tag}/model",tuner=tuner, feature_names = list(X_.columns), overwrite=True)
    Storage.write_pd(key=f"{root_tag}/best_params", df = pd.DataFrame([tuner.best_params]).T, overwrite=True)
    print('Done with training model')
    return tuner

tuner = Run_tuner(X_=Sig_vs_BKG_SB_splits['X_train_scaled'],y_=Sig_vs_BKG_SB_splits['y_train'],root_tag='BDT_SB_iter2')



# %%
Sig_vs_BKG_SR = pd.concat([SIG_MC,BKG_SR,BKG_MC.query(Utils.selection_cuts()['MC2'])])
Sig_vs_BKG_SR_MC_test = pd.concat([MC_tests['SR'],MC_tests['MC']])
Sig_vs_BKG_SR_splits = train_test_split_stratified(Sig_vs_BKG_SR)

Utils.fit_scaler_to_train(Storage,Sig_vs_BKG_SR_splits['X_train'][features],root_tag='BDT_SR_iter2')
Sig_vs_BKG_SR_splits['X_train_scaled'] = Utils.pd_scale(Storage,Sig_vs_BKG_SR_splits['X_train'][features], root_tag= 'BDT_SR_iter2', target_tag = 'X_train')
Sig_vs_BKG_SR_splits['X_test_scaled'] = Utils.pd_scale(Storage,Sig_vs_BKG_SR_splits['X_test'][features], root_tag= 'BDT_SR_iter2', target_tag = 'X_test')


# %%

def Run_tuner(X_,y_,Storage=Storage,root_tag='BDT_SR'):

    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"

    np.random.seed(42)
    #print(f"Training LGBM with {Storage.read_pd(key=f"{root_tag}/X_train").shape[1]} features")
    tuner = verstack.LGBMTuner(metric = 'log_loss',trials=100, visualization = False, seed = 42,verbosity=1)
    
    tuner.fit(X=X_,y=y_.astype(int).squeeze())

    Storage.write_pd(key=f"{root_tag}/tuner", df = tuner.study.trials_dataframe(attrs=('number', 'value', 'params', 'state','intermediate_values'),multi_index=True),overwrite=True)
    Storage.write_pd(key=f"{root_tag}/tuner_param_imp", df = pd.DataFrame([dict(optuna.importance.get_param_importances(tuner.study).items())]).T.rename(columns={0:'value'}),overwrite=True)
    Storage.save_lgbm(key=f"{root_tag}/model",tuner=tuner, feature_names = list(X_.columns), overwrite=True)

    print('Done with training model')
    return tuner

tuner = Run_tuner(X_=Sig_vs_BKG_SR_splits['X_train_scaled'],y_=Sig_vs_BKG_SR_splits['y_train'],root_tag='BDT_SR_iter2')



# %%
def Data_loader():
    N_times_signal = 4
    print('Loading MC signal..')
    SIG_MC=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['USR'], columns=None, use_threads=True, storage_options=None)
    SIG_MC = SIG_MC.query(Utils.selection_cuts()['usr_lowq2'])
    SIG_MC.drop(SIG_MC[~((SIG_MC.BeeKst_isTrueNonresBd==1) | (SIG_MC.BeeKst_isTrueNonresBdbar==1))].index,inplace=True)
    SIG_MC.drop(SIG_MC[(SIG_MC.BeeKst_isTrueNonresBd==1)].index[N_times_signal*config['ML_config']['N_Signal']['Bd']:],inplace=True)
    SIG_MC.drop(SIG_MC[(SIG_MC.BeeKst_isTrueNonresBdbar==1)].index[N_times_signal*config['ML_config']['N_Signal']['BdBar']:],inplace=True)

    label = (SIG_MC.info_sample == 0)*0
    for i in config['labels B_VS_BBar']:
        label += (SIG_MC.info_sample == str(i))*config['labels B_VS_BBar'][str(i)]
    SIG_MC['label'] = label

    return SIG_MC

B_vs_Bbar = Data_loader()

B_vs_Bbar = pd.concat([B_vs_Bbar])
B_vs_Bbar_splits = train_test_split_stratified(B_vs_Bbar)

Utils.fit_scaler_to_train(Storage,B_vs_Bbar_splits['X_train'][features],root_tag='BDT_BBbar_iter2')
B_vs_Bbar_splits['X_train_scaled'] = Utils.pd_scale(Storage,B_vs_Bbar_splits['X_train'][features], root_tag= 'BDT_BBbar_iter2', target_tag = 'X_train')
B_vs_Bbar_splits['X_test_scaled'] = Utils.pd_scale(Storage,B_vs_Bbar_splits['X_test'][features], root_tag= 'BDT_BBbar_iter2', target_tag = 'X_test')

# %%

def Run_tuner(X_,y_,Storage=Storage,root_tag='BDT_MC'):

    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"

    np.random.seed(42)
    #print(f"Training LGBM with {Storage.read_pd(key=f"{root_tag}/X_train").shape[1]} features")
    tuner = verstack.LGBMTuner(metric = 'log_loss',trials=100, visualization = False, seed = 42,verbosity=1)
    
    tuner.fit(X=X_,y=y_.astype(int).squeeze())

    Storage.write_pd(key=f"{root_tag}/tuner", df = tuner.study.trials_dataframe(attrs=('number', 'value', 'params', 'state','intermediate_values'),multi_index=True),overwrite=True)
    Storage.write_pd(key=f"{root_tag}/tuner_param_imp", df = pd.DataFrame([dict(optuna.importance.get_param_importances(tuner.study).items())]).T.rename(columns={0:'value'}),overwrite=True)
    Storage.save_lgbm(key=f"{root_tag}/model",tuner=tuner, feature_names = list(X_.columns), overwrite=True)

    print('Done with training model')
    return tuner

tuner = Run_tuner(X_=B_vs_Bbar_splits['X_train_scaled'],y_=B_vs_Bbar_splits['y_train'],root_tag='BDT_BBbar_iter2')
