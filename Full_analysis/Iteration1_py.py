# %%
print('Kernel Initialized')

# %%
# Imports
import sys
sys.path.append("..")
import pandas as pd
import numpy as np
from utils import Utils
import plot_utils as plotting
import toml
from sklearn import preprocessing, model_selection, metrics
import seaborn as sns
import random

import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
plt.style.use("seaborn-v0_8-whitegrid")
#plt.style.use("seaborn-v0_8")
#import mplhep as hep
#hep.style.use(hep.style.ATLAS)
from scipy.special import logit, expit
import os

import verstack
import optuna

import scipy
import lightgbm

random.seed(42)

# %%
config = toml.load('config.toml')
Storage = Utils.HDF5_IO(config['PATHS']['Storage'])

# %%
def Data_loader():
    print('Loading MC signal..')
    B_masses = ['B_mass', 'B_mass_true','B_Bbar_mass_closer_PDG','Bbar_mass','BeeKst_B_mass','BeeKst_Bbar_mass','B_mass_Kstar_mass_closer']

    SIG_MC=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['USR'], columns=None, use_threads=True, storage_options=None)
    SIG_MC = SIG_MC.query(Utils.selection_cuts()['usr_lowq2'])
    SIG_MC.drop(SIG_MC[~((SIG_MC.BeeKst_isTrueNonresBd==1) | (SIG_MC.BeeKst_isTrueNonresBdbar==1))].index,inplace=True)
    SIG_MC.drop(SIG_MC[(SIG_MC.BeeKst_isTrueNonresBd==1)].index[config['ML_config']['N_Signal']['Bd']:],inplace=True)
    SIG_MC.drop(SIG_MC[(SIG_MC.BeeKst_isTrueNonresBdbar==1)].index[config['ML_config']['N_Signal']['BdBar']:],inplace=True)


    label = (SIG_MC.info_sample == 0)*0
    for i in config['labels Sig_VS_Bkg']:
        label += (SIG_MC.info_sample == str(i))*config['labels Sig_VS_Bkg'][str(i)]

    #SIG_MC=SIG_MC.drop(columns=info_features)
    #SIG_MC=SIG_MC.drop(columns=B_masses)
    print('Colomns of only on value is removed..')
    SIG_MC=SIG_MC.drop(SIG_MC.std()[(SIG_MC.std() == 0)].index, axis=1)
    print('Dublecates is removed..')
    SIG_MC=SIG_MC.loc[:,~SIG_MC.T.duplicated(keep='last')]
    SIG_MC['label'] = label
    SIG_MC['label'].astype(int)


    print('Loading background sidebands..')
    BKG_SB=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['SB'], columns=None, use_threads=True, storage_options=None)
    BKG_SB = BKG_SB.query(Utils.selection_cuts()['nn1trb_lowq2'])
    BKG_SB.drop(BKG_SB.index[config['ML_config']['N_background']:],inplace=True)
    label = (BKG_SB.info_sample == 0)*0
    for i in config['labels Sig_VS_Bkg']:
        label += (BKG_SB.info_sample == str(i))*config['labels Sig_VS_Bkg'][str(i)]
    BKG_SB['label'] = label
    BKG_SB['label'].astype(int)

    print('Loading background signal region..')
    BKG_SR=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['SR'], columns=None, use_threads=True, storage_options=None)
    BKG_SR = BKG_SR.query(Utils.selection_cuts()['nn2trb_lowq2'])
    BKG_SR.drop(BKG_SR.index[config['ML_config']['N_background']:],inplace=True)
    label = (BKG_SR.info_sample == 0)*0
    for i in config['labels Sig_VS_Bkg']:
        label += (BKG_SR.info_sample == str(i))*config['labels Sig_VS_Bkg'][str(i)]
    BKG_SR['label'] = label
    BKG_SR['label'].astype(int)

    print('Loading MC background..')
    BKG_MC=pd.read_feather(path=config['train_files']['Bkg_MC'], columns=None, use_threads=True, storage_options=None)
    BKG_MC = BKG_MC.query(Utils.selection_cuts()['main'])
    BKG_MC.info_sample = 'BKG_MC'
    label = (BKG_MC.info_sample == 0)*0
    for i in config['labels Sig_VS_Bkg']:
        label += (BKG_MC.info_sample == str(i))*config['labels Sig_VS_Bkg'][str(i)]
    BKG_MC['label'] = label
    BKG_MC['label'].astype(int)


    # Generate tests..
    print('Loading tests..')
    BKG_MC_test=pd.read_feather(path=config['train_files']['Bkg_MC'], columns=None, use_threads=True, storage_options=None)
    BKG_MC_test = BKG_MC_test.query(Utils.selection_cuts()['main'])
    BKG_MC_test.info_sample = 'BKG_MC_test'
    BKG_MC_test['label'] = 1


    BKG_SB_test=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['SB'], columns=None, use_threads=True, storage_options=None)
    BKG_SB_test = BKG_SB_test.query(Utils.selection_cuts()['nn1trb_lowq2'])
    BKG_SB_test.drop(BKG_SB_test.index[len(BKG_MC_test):],inplace=True)
    BKG_SB_test['label'] = 0

    BKG_SR_test=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['SR'], columns=None, use_threads=True, storage_options=None)
    BKG_SR_test = BKG_SR_test.query(Utils.selection_cuts()['nn2trb_lowq2'])
    BKG_SR_test.drop(BKG_SR_test.index[len(BKG_MC_test):],inplace=True)
    BKG_SR_test['label'] = 0


    return SIG_MC, BKG_SB, BKG_SR, BKG_MC, {'SB':BKG_SB_test, 'SR': BKG_SR_test, 'MC': BKG_MC_test }

SIG_MC, BKG_SB, BKG_SR, BKG_MC, MC_tests = Data_loader()


# %%
info_features = config['features']['info_features'] + ['track_multiplicity_after_cuts', 'electron_multiplicity_after_cuts', 'entry', 'subentry','mc_channel_number','event_number']
B_masses = ['B_mass', 'B_mass_true','B_Bbar_mass_closer_PDG','Bbar_mass','BeeKst_B_mass','BeeKst_Bbar_mass','B_mass_Kstar_mass_closer']
non_contributing_features = []
bad_features = ['diElectron_mass', 'BeeKst_PV_minZ0_y', 'BeeKst_PV_minA0_y', 'BeeKst_PV_minZ0_x']
feature_remove = ['label'] + info_features + B_masses + non_contributing_features + bad_features

features = [i for i in list(SIG_MC.columns) if i not in feature_remove]

# %%
remove_feature_for_plot = [i for i in list(SIG_MC.columns) if i in B_masses+bad_features]
mpl.rc('font', **{'family' : 'stixgeneral'})  # pass in the font dict as kwargs
mpl.rc('text', **{'usetex': False})
mpl.rc('mathtext', **{'fontset': 'stix'})


# %%
def train_test_split_stratified(X):
    random.seed(42)
    ratios = {
        'train': 0.7,
        'valid': 0.0,
        'test': 0.30}
    np.random.seed(42)
    X_train, X_rem, y_train, y_rem = model_selection.train_test_split(X, X.label, train_size=ratios['train'],stratify=X.label, shuffle=True,random_state=42)
    if ratios['valid'] == 0:
        X_test = X_rem
        y_test = y_rem
        return {'X_train': X_train, 'y_train': y_train, 'X_test': X_test, 'y_test': y_test}
    else:
        X_valid, X_test, y_valid, y_test = model_selection.train_test_split(X_rem, y_rem, test_size=(ratios['test']/(1-ratios['train'])),stratify=y_rem,shuffle=True,random_state=42)
        return {'X_train': X_train, 'y_train': y_train, 'X_valid': X_valid, 'y_valid': y_valid, 'X_test': X_test, 'y_test': y_test}


# %%
Sig_vs_BKG_SR = pd.concat([SIG_MC,BKG_SR,BKG_MC.query(Utils.selection_cuts()['MC2'])])
Sig_vs_BKG_SR_MC_test = pd.concat([MC_tests['SR'],MC_tests['MC']])
Sig_vs_BKG_SR_splits = train_test_split_stratified(Sig_vs_BKG_SR)

Utils.fit_scaler_to_train(Storage,Sig_vs_BKG_SR_splits['X_train'][features],root_tag='BDT_SR_iter1')
Sig_vs_BKG_SR_splits['X_train_scaled'] = Utils.pd_scale(Storage,Sig_vs_BKG_SR_splits['X_train'][features], root_tag= 'BDT_SR_iter1', target_tag = 'X_train')
Sig_vs_BKG_SR_splits['X_test_scaled'] = Utils.pd_scale(Storage,Sig_vs_BKG_SR_splits['X_test'][features], root_tag= 'BDT_SR_iter1', target_tag = 'X_test')




def Run_tuner(X_,y_,Storage=Storage,root_tag='BDT_SR'):

    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"

    np.random.seed(42)
    #print(f"Training LGBM with {Storage.read_pd(key=f"{root_tag}/X_train").shape[1]} features")
    tuner = verstack.LGBMTuner(metric = 'log_loss',trials=100, visualization = False, seed = 42,verbosity=1)
    
    tuner.fit(X=X_,y=y_.astype(int).squeeze())

    Storage.write_pd(key=f"{root_tag}/tuner", df = tuner.study.trials_dataframe(attrs=('number', 'value', 'params', 'state','intermediate_values'),multi_index=True),overwrite=True)
    Storage.write_pd(key=f"{root_tag}/tuner_param_imp", df = pd.DataFrame([dict(optuna.importance.get_param_importances(tuner.study).items())]).T.rename(columns={0:'value'}),overwrite=True)
    Storage.save_lgbm(key=f"{root_tag}/model",tuner=tuner, feature_names = list(X_.columns), overwrite=True)

    print('Done with training model')
    return tuner

tuner = Run_tuner(X_=Sig_vs_BKG_SR_splits['X_train_scaled'],y_=Sig_vs_BKG_SR_splits['y_train'],root_tag='BDT_SR_iter1')


# %%
def Data_loader():
    N_times_signal = 4
    print('Loading MC signal..')
    SIG_MC=pd.read_feather(path=config['PATHS']['feather']+ config['train_files']['USR'], columns=None, use_threads=True, storage_options=None)
    SIG_MC = SIG_MC.query(Utils.selection_cuts()['usr_lowq2'])
    SIG_MC.drop(SIG_MC[~((SIG_MC.BeeKst_isTrueNonresBd==1) | (SIG_MC.BeeKst_isTrueNonresBdbar==1))].index,inplace=True)
    SIG_MC.drop(SIG_MC[(SIG_MC.BeeKst_isTrueNonresBd==1)].index[N_times_signal*config['ML_config']['N_Signal']['Bd']:],inplace=True)
    SIG_MC.drop(SIG_MC[(SIG_MC.BeeKst_isTrueNonresBdbar==1)].index[N_times_signal*config['ML_config']['N_Signal']['BdBar']:],inplace=True)

    label = (SIG_MC.info_sample == 0)*0
    for i in config['labels B_VS_BBar']:
        label += (SIG_MC.info_sample == str(i))*config['labels B_VS_BBar'][str(i)]
    SIG_MC['label'] = label

    return SIG_MC

B_vs_Bbar = Data_loader()

B_vs_Bbar = pd.concat([B_vs_Bbar])
B_vs_Bbar_splits = train_test_split_stratified(B_vs_Bbar)

Utils.fit_scaler_to_train(Storage,B_vs_Bbar_splits['X_train'][features],root_tag='BDT_BBbar_iter1')
B_vs_Bbar_splits['X_train_scaled'] = Utils.pd_scale(Storage,B_vs_Bbar_splits['X_train'][features], root_tag= 'BDT_BBbar_iter1', target_tag = 'X_train')
B_vs_Bbar_splits['X_test_scaled'] = Utils.pd_scale(Storage,B_vs_Bbar_splits['X_test'][features], root_tag= 'BDT_BBbar_iter1', target_tag = 'X_test')


def Run_tuner(X_,y_,Storage=Storage,root_tag='BDT_MC'):

    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"

    np.random.seed(42)
    #print(f"Training LGBM with {Storage.read_pd(key=f"{root_tag}/X_train").shape[1]} features")
    tuner = verstack.LGBMTuner(metric = 'log_loss',trials=100, visualization = False, seed = 42,verbosity=1)
    
    tuner.fit(X=X_,y=y_.astype(int).squeeze())

    Storage.write_pd(key=f"{root_tag}/tuner", df = tuner.study.trials_dataframe(attrs=('number', 'value', 'params', 'state','intermediate_values'),multi_index=True),overwrite=True)
    Storage.write_pd(key=f"{root_tag}/tuner_param_imp", df = pd.DataFrame([dict(optuna.importance.get_param_importances(tuner.study).items())]).T.rename(columns={0:'value'}),overwrite=True)
    Storage.save_lgbm(key=f"{root_tag}/model",tuner=tuner, feature_names = list(X_.columns), overwrite=True)

    print('Done with training model')
    return tuner

tuner = Run_tuner(X_=B_vs_Bbar_splits['X_train_scaled'],y_=B_vs_Bbar_splits['y_train'],root_tag='BDT_BBbar_iter1')

# %%



