import time
import os
import random
import pandas as pd
import numpy as np
import uproot
from scipy import stats
import vector
random.seed(42)
import datetime
import ROOT
import warnings
from multiprocessing import Process, Queue

# cd RK_Star_Analysis/Analysis/GNN_challenger/
# conda activate RK_star
# rm nohup.out
# nohup python3 -u Root_to_feather.py &
# jobs -l
# ps xw


class Timer():
    def __init__(self):
        pass

    def start(self):
        self.start_time = datetime.datetime.now()
    
    def stop(self):
        self.stop_time = datetime.datetime.now()
        result = self.stop_time -self.start_time
        print(f"[{result.seconds // 3600} hours | {result.seconds // 60} minutes | {result.seconds % 60} seconds")


warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)


PATH_file_name= 'MC_signal_train2.ftr'
feather_folder_path = '/groups/hep/kvh318/RK_Star_Analysis/RKStar_Analysis/data/feather_data/'





def prep_multiprocess(root_PATH, N_process:int=4,max_entries:int = None):
    Root = uproot.open(f"{root_PATH}:Nominal/BaseSelection_KStaree_BeeKstSelection")
    N_entries = Root.num_entries
    
    print()
    print('----------------------------------------------------------------------------')
    print("Uproot'ing: ntuple"+root_PATH.split("ntuple",1)[1], f" into {N_process} subprocesses")
    print('----------------------------------------------------------------------------')
    print()
    print()


    process_array=np.linspace(0, N_entries, num=N_process+1,dtype=int)


    if max_entries != None:
        process_array=np.linspace(0, max_entries, num=N_process+1,dtype=int)


    return N_entries,process_array




def root_file_opener(root_PATH,additional_filter:str = "", start_entry:int=None, stop_entry:int=None):
        Root = uproot.open(f"{root_PATH}:Nominal/BaseSelection_KStaree_BeeKstSelection")

        
        filter_name_BeeKst= "/accepted_triggers|event_number|weight|mc_channel_number|"+"BeeKst"+"_.+/",
        filter_name_InDet= "/accepted_triggers|event_number|weight|mc_channel_number|InDet_pass.+|InDet_pT|InDet_eta/",
        filter_name_GSF= "/accepted_triggers|event_number|weight|mc_channel_number|GSF_pass.+|GSF_pT|GSF_eta/",

        # Base Selection:
       
        column_filter = "((BeeKst_electron0_pT > 5000) & (BeeKst_electron1_pT > 5000))"
        column_filter += " & ((abs(BeeKst_electron0_eta ) < 2.5) & (abs(BeeKst_electron1_eta) < 2.5))"
        column_filter += " & ((BeeKst_meson0_pT > 500) & (BeeKst_meson1_pT > 500))"
        column_filter += " & ((abs(BeeKst_meson0_eta) < 2.5) & (abs(BeeKst_meson1_eta) < 2.5))"
        column_filter += " & (BeeKst_diElectron_mass < 7000)"
        column_filter += " & (BeeKst_diElectron_gsf_id_isOK == 1)"
        BmPassed = "((BeeKst_B_mass >= 3000) & (BeeKst_B_mass <= 6500))"
        BbmPassed = "((BeeKst_Bbar_mass >= 3000) & (BeeKst_Bbar_mass <= 6500))"
        column_filter += f" & ({BmPassed} | {BbmPassed})"
        column_filter += f" & (({BmPassed} & (BeeKst_kaonPion_mass > 690) & (BeeKst_kaonPion_mass < 1110)) | ({BbmPassed} & (BeeKst_pionKaon_mass > 690) & (BeeKst_pionKaon_mass < 1110)))"
        column_filter += " & ((BeeKst_electron0_passLoose == 1) & (BeeKst_electron1_passLoose == 1) & (BeeKst_meson0_passLoose == 1) & (BeeKst_meson1_passLoose == 1) & (BeeKst_electron0_passLooseElectron == 1) & (BeeKst_electron1_passLooseElectron == 1))"
        column_filter += "& (sqrt( ((BeeKst_electron0_eta - BeeKst_electron1_eta)**2) + ((BeeKst_electron0_phi - BeeKst_electron1_phi)**2) ) > 0.1 )"



        column_filter += additional_filter
        
        #column_filter_INPUT = column_filter
        
        vtx_BeeKst = Root.arrays(
            expressions = None,
            cut = column_filter,
            filter_name = filter_name_BeeKst,
            library = 'pd',
            entry_start = start_entry,
            entry_stop = stop_entry)

        vtx_GSF = Root.arrays(
            expressions = None,
            filter_name = filter_name_GSF,
            library = 'pd',
            entry_start = start_entry,
            entry_stop = stop_entry)

        vtx_InDet = Root.arrays(
            expressions = None,
            filter_name = filter_name_InDet,
            library = 'pd',
            entry_start = start_entry,
            entry_stop = stop_entry)


        return vtx_BeeKst, vtx_GSF, vtx_InDet

def save_to_feater(data, PATH:str):
    data.reset_index().to_feather(feather_folder_path+ PATH)

def distance_significance(vtx1, vtx1_err, vtx2, vtx2_err):   # Calculate distance significance of two vertices
    dist_sign = np.full_like(vtx1.x.to_numpy(), -999.)
    vect = vtx2 - vtx1
    d2 = vect.mag2.to_numpy()
    x2 = vect.x * vect.x
    y2 = vect.y * vect.y
    z2 = vect.z * vect.z

    x_err2 = vtx1_err.x * vtx1_err.x + vtx2_err.x * vtx2_err.x
    y_err2 = vtx1_err.y * vtx1_err.y + vtx2_err.y * vtx2_err.y
    z_err2 = vtx1_err.z * vtx1_err.z + vtx2_err.z * vtx2_err.z

    denom = np.sqrt( x2 * x_err2 + y2 * y_err2 + z2 * z_err2 ).to_numpy()
    dist_sign[(denom > 0)] = d2 / denom

    return dist_sign

def cos_Theta_error(a, a_err, b, b_err):   # Calculate cos_theta error, where theta is the angle between vector a and b
    ab = a.dot(b)
    a2 = a.dot(a)
    b2 = b.dot(b)
    ab2 = a2 * b2
    ab3 = np.power(ab2, 3/2)

    dax2_axerr2 = np.power( ( b.x * ab2 - a.x * b2 * ab ) * a_err.x / ab3 , 2)
    day2_ayerr2 = np.power( ( b.y * ab2 - a.y * b2 * ab ) * a_err.y / ab3 , 2)
    daz2_azerr2 = np.power( ( b.z * ab2 - a.z * b2 * ab ) * a_err.z / ab3 , 2)

    dbx2_bxerr2 = np.power( ( a.x * ab2 - b.x * a2 * ab ) * b_err.x / ab3 , 2)
    dby2_byerr2 = np.power( ( a.y * ab2 - b.y * a2 * ab ) * b_err.y / ab3 , 2)
    dbz2_bzerr2 = np.power( ( a.z * ab2 - b.z * a2 * ab ) * b_err.z / ab3 , 2)

    return np.sqrt( dax2_axerr2 + day2_ayerr2 + daz2_azerr2 + dbx2_bxerr2 + dby2_byerr2 + dbz2_bzerr2 )

def remove_null_errors(df):  # Remove row if there are any null errors in it
    error_list = ["BeeKst_a0xy_maxSumPt2_err", "BeeKst_a0xy_minA0_err","BeeKst_a0xy_minZ0_err",
                        "BeeKst_Lxy_maxSumPt2_err", "BeeKst_Lxy_minA0_err", "BeeKst_Lxy_minZ0_err", 
                        "BeeKst_a0_maxSumPt2_err", "BeeKst_a0_minA0_err", "BeeKst_a0_minZ0_err",
                        "BeeKst_z0_maxSumPt2_err", "BeeKst_z0_minA0_err", "BeeKst_z0_minZ0_err",
                        "BeeKst_B_tau_constM_PVMaxSumPt2_err", "BeeKst_B_tau_constM_PVMinA0_err",
                        "BeeKst_B_tau_constM_PVMinZ0_err", "BeeKst_B_tau_invM_PVMaxSumPt2_err",
                        "BeeKst_B_tau_invM_PVMinA0_err", "BeeKst_B_tau_invM_PVMinZ0_err", 
                        "BeeKst_Bbar_tau_constM_PVMaxSumPt2_err", "BeeKst_Bbar_tau_constM_PVMinA0_err",
                        "BeeKst_Bbar_tau_constM_PVMinZ0_err", "BeeKst_Bbar_tau_invM_PVMaxSumPt2_err",
                        "BeeKst_Bbar_tau_invM_PVMinA0_err", "BeeKst_Bbar_tau_invM_PVMinZ0_err",
                        "BeeKst_B_mass_err", "BeeKst_Bbar_mass_err"]
    
    #print("Removing candidates with null errors ...")
    for error in error_list:
        if error in df.columns.values:
            df = df[df[error] != 0]
    #print()

def remove_unfilled_variables(df):  # Remove row if there are any unfilled variables
    #print("Removing candidates with unfilled variables (NaN) ...")
    df.replace([np.inf, -np.inf], np.nan, inplace = True)
    df.dropna(inplace = True)
    #print()

def remove_duplicated_candidates(df, columns_to_check = []): # Remove duplicated row
    #print("Removing duplicated candidates...")
    if len(columns_to_check) == 0:
        df.drop_duplicates(inplace = True)
    else:
        df.drop_duplicates(subset = columns_to_check, inplace = True)
    #print()


def add_features(data, data_GSF, data_InDet ):
    #data = vtx[0]
    #data_GSF = vtx[1]
    #data_InDet = vtx[2]

    # Track multiplicity
    # ===========================
    #print(data_InDet.InDet_pT)
    track_multiplicity    = ( (data_InDet.InDet_pT >= 500).values * (abs(data_InDet.InDet_eta) <= 2.5).values * (data_InDet.InDet_passLoose == 1).values ).sum()
    electron_multiplicity = ( (data_GSF.GSF_pT >= 5000).values * (abs(data_GSF.GSF_eta) <= 2.5).values * (data_GSF.GSF_passLoose == 1).values * (data_GSF.GSF_passLooseElectron == 1).values ).sum()

    # Sample helper functions
    # =======================
    if len(data.mc_channel_number) > 0:
        mc_sample     = "data" if (data.mc_channel_number.iloc[0] < 0).any() else str(data.mc_channel_number.iloc[0])
    else:
        mc_sample = "error"
    sample_weight = 1


    # VTX helper functions
    # ====================
    # "== 1" is needed to convert int to bool
    vtx_truth = (mc_sample == "300590") * (data.BeeKst_isTrueNonresBd == 1) + (mc_sample == "300591") * (data.BeeKst_isTrueNonresBdbar == 1) + (mc_sample == "300592") * (data.BeeKst_isTrueResBd == 1) + (mc_sample == "300593") * (data.BeeKst_isTrueResBdbar == 1)    

    vtx_e0_p4 = vector.zip({'pt': data.BeeKst_electron0_pT, 'eta':  data.BeeKst_electron0_eta, 'phi': data.BeeKst_electron0_phi, 'mass': 0.511})
    vtx_e1_p4 = vector.zip({'pt': data.BeeKst_electron1_pT, 'eta':  data.BeeKst_electron1_eta, 'phi': data.BeeKst_electron1_phi, 'mass': 0.511})
    vtx_m0_pi_p4 = vector.zip({'pt': data.BeeKst_meson0_pT, 'eta': data.BeeKst_meson0_eta, 'phi': data.BeeKst_meson0_phi, 'mass': 139.57})
    vtx_m0_K_p4 = vector.zip({'pt': data.BeeKst_meson0_pT, 'eta': data.BeeKst_meson0_eta, 'phi': data.BeeKst_meson0_phi, 'mass': 493.677})
    vtx_m1_pi_p4 = vector.zip({'pt':data.BeeKst_meson1_pT, 'eta': data.BeeKst_meson1_eta, 'phi': data.BeeKst_meson1_phi, 'mass': 139.57})
    vtx_m1_K_p4 = vector.zip({'pt':data.BeeKst_meson1_pT, 'eta': data.BeeKst_meson1_eta, 'phi': data.BeeKst_meson1_phi, 'mass': 493.677})

    vtx_diLepton_p4     = vtx_e0_p4 + vtx_e1_p4
    vtx_Kpi_p4        = vtx_m0_K_p4 + vtx_m1_pi_p4
    vtx_Bd_p4         = vtx_diLepton_p4 + vtx_Kpi_p4

    vtx_Bd_pv            = vector.zip({'x': data.BeeKst_PV_minA0_x, 'y': data.BeeKst_PV_minA0_y, 'z': data.BeeKst_PV_minA0_z})
    vtx_Bd_p3_err        = vector.zip({'x': data.BeeKst_B_mass * 0, 'y': data.BeeKst_B_mass * 0, 'z': data.BeeKst_B_mass * 0 })
    vtx_Bd_pv_err        = vector.zip({'x': data.BeeKst_PV_minA0_x_err, 'y': data.BeeKst_PV_minA0_y_err, 'z': data.BeeKst_PV_minA0_z_err})
    vtx_Bd_vtx           = vector.zip({'x': data.BeeKst_x, 'y': data.BeeKst_y, 'z': data.BeeKst_z})
    vtx_Bd_vtx_err       = vector.zip({'x': data.BeeKst_x_err, 'y': data.BeeKst_y_err, 'z': data.BeeKst_z_err})
    vtx_diMeson_vtx      = vector.zip({'x': data.BeeKst_diMeson_vtx_x, 'y': data.BeeKst_diMeson_vtx_y, 'z': data.BeeKst_diMeson_vtx_z})
    vtx_diMeson_vtx_err  = vector.zip({'x': data.BeeKst_diMeson_vtx_x_err, 'y': data.BeeKst_diMeson_vtx_y_err, 'z': data.BeeKst_diMeson_vtx_z_err})
    vtx_diLepton_vtx     = vector.zip({'x': data.BeeKst_diElectron_vtx_x, 'y': data.BeeKst_diElectron_vtx_y, 'z': data.BeeKst_diElectron_vtx_z})
    vtx_diLepton_vtx_err = vector.zip({'x': data.BeeKst_diElectron_vtx_x_err, 'y': data.BeeKst_diElectron_vtx_y_err, 'z': data.BeeKst_diElectron_vtx_z_err})

    # Using 4-vector because of the pre-defined functions -> dummy mass
    vtx_pv2B            = vtx_Bd_vtx - vtx_Bd_pv
    vtx_pv2B_err        = vector.zip({'x': np.sqrt( vtx_Bd_vtx_err.x * vtx_Bd_vtx_err.x + vtx_Bd_pv_err.x * vtx_Bd_pv_err.x ),'y': np.sqrt( vtx_Bd_vtx_err.y * vtx_Bd_vtx_err.y + vtx_Bd_pv_err.y * vtx_Bd_pv_err.y ),'z': np.sqrt( vtx_Bd_vtx_err.z * vtx_Bd_vtx_err.z + vtx_Bd_pv_err.z * vtx_Bd_pv_err.z ) })
    vtx_B2diMeson       = vtx_diMeson_vtx - vtx_Bd_vtx
    vtx_B2diMeson_v4    = vector.zip({'x': vtx_B2diMeson.x, 'y': vtx_B2diMeson.y, 'z': vtx_B2diMeson.z, 't': 0.})
    vtx_B2diLepton      = vtx_diLepton_vtx - vtx_Bd_vtx
    vtx_B2diLepton_v4    = vector.zip({'x': vtx_B2diLepton.x, 'y': vtx_B2diLepton.y, 'z': vtx_B2diLepton.z, 't': 0.})

    vtx_n1     = vtx_B2diLepton.cross(vtx_B2diMeson)
    vtx_n1_pp  = vtx_diLepton_p4.cross(vtx_Kpi_p4)
    vtx_n2_pee = vtx_n1.cross(vtx_diLepton_p4)
    vtx_n2_ee  = vtx_e0_p4.cross(vtx_e1_p4)
    vtx_n2_pmm = vtx_n1.cross(vtx_Kpi_p4)
    vtx_n2_mm  = vtx_m0_K_p4.cross(vtx_m1_pi_p4)
    vtx_n3_pee = vtx_n2_pee.cross(vtx_n1)
    vtx_n3_pmm = vtx_n2_pmm.cross(vtx_n1)


    df = pd.DataFrame(vtx_Bd_p4.mag,columns=["B_p"],index=data.index)
    df["info_is_true_nonres_Bd"]    = ( (mc_sample == "300590") * data.BeeKst_isTrueNonresBd )*1
    df["info_is_true_nonres_BdBar"] = ( (mc_sample == "300591") * data.BeeKst_isTrueNonresBdbar)*1
    df["info_is_true_res_Bd"]       = ( (mc_sample == "300592") * data.BeeKst_isTrueResBd)*1
    df["info_is_true_res_BdBar"]    = ( (mc_sample == "300593") * data.BeeKst_isTrueResBdbar)*1
    
    
    df["diElectron_p"]  = vtx_diLepton_p4.mag
    df["diMeson_p"]     = vtx_Kpi_p4.mag
    df["positron_p"]    = vtx_e0_p4.mag
    df["electron_p"]    = vtx_e1_p4.mag
    df["trackPlus_p"]   = vtx_m0_pi_p4.mag
    df["trackMinus_p"]  = vtx_m1_pi_p4.mag

    df["B_pz"]          = vtx_Bd_p4.z
    df["diElectron_pz"] = vtx_diLepton_p4.z
    df["diMeson_pz"]    = vtx_Kpi_p4.z
    df["positron_pz"]   = vtx_e0_p4.z
    df["electron_pz"]   = vtx_e1_p4.z
    df["trackPlus_pz"]  = vtx_m0_pi_p4.z
    df["trackMinus_pz"] = vtx_m1_pi_p4.z

    df["B_pT"]          = vtx_Bd_p4.pt
    df["diElectron_pT"] = vtx_diLepton_p4.pt
    df["diMeson_pT"]    = vtx_Kpi_p4.pt
    df["positron_pT"]   = data.BeeKst_electron0_pT
    df["electron_pT"]   = data.BeeKst_electron1_pT
    df["trackPlus_pT"]  = data.BeeKst_meson0_pT
    df["trackMinus_pT"] = data.BeeKst_meson1_pT

    df["B_eta"]          = vtx_Bd_p4.eta
    df["diElectron_eta"] = vtx_diLepton_p4.eta
    df["diMeson_eta"]    = vtx_Kpi_p4.eta
    df["positron_eta"]   = data.BeeKst_electron0_eta
    df["electron_eta"]   = data.BeeKst_electron1_eta
    df["trackPlus_eta"]  = data.BeeKst_meson0_eta
    df["trackMinus_eta"] = data.BeeKst_meson1_eta

    df["B_phi"]          = vtx_Bd_p4.phi
    df["diElectron_phi"] = vtx_diLepton_p4.phi
    df["diMeson_phi"]    = vtx_Kpi_p4.phi
    df["positron_phi"]   = data.BeeKst_electron0_phi
    df["electron_phi"]   = data.BeeKst_electron1_phi
    df["trackPlus_phi"]  = data.BeeKst_meson0_phi
    df["trackMinus_phi"] = data.BeeKst_meson1_phi

    df["energy_positron"]           = vtx_e0_p4.E
    df["energy_electron"]            = vtx_e1_p4.E
    df["energy_trackPlus_pion"]     = vtx_m0_pi_p4.E
    df["energy_trackMinus_pion"]    = vtx_m1_pi_p4.E
    df["energy_trackPlus_kaon"]     = vtx_m0_K_p4.E
    df["energy_trackMinus_kaon"]    = vtx_m1_K_p4.E

    df["diElectron_mass"]       = data.BeeKst_diElectron_mass
    df["diMeson_Kpi_mass"]      = data.BeeKst_kaonPion_mass
    df["diMeson_piK_mass"]      = data.BeeKst_pionKaon_mass
    df["B_mass"]                = data.BeeKst_B_mass
    df["Bbar_mass"]             = data.BeeKst_Bbar_mass
    df["B_Bbar_mass_closer_PDG"]                = ( ( abs(data.BeeKst_B_mass - 5279.64) < abs(data.BeeKst_Bbar_mass - 5279.64) ) * data.BeeKst_B_mass + ( abs(data.BeeKst_B_mass - 5279.64) >= abs(data.BeeKst_Bbar_mass - 5279.64) ) * data.BeeKst_Bbar_mass )
    df["B_mass_true"] = ( (df["info_is_true_nonres_Bd"]) | (df["info_is_true_res_Bd"]) ) * df["B_mass"] + ( (df["info_is_true_nonres_BdBar"]) | (df["info_is_true_res_BdBar"]) ) * df["Bbar_mass"]
    df["B_mass_Kstar_mass_closer"]              = ( abs(df["diMeson_Kpi_mass"] - 891.66) < abs(df["diMeson_piK_mass"] - 891.66) ) * df["B_mass"] + ( abs(df["diMeson_Kpi_mass"] - 891.66) >= abs(df["diMeson_piK_mass"] - 891.66) ) * df["Bbar_mass"]    
    df["mass_pionKaon_kaonPion_ratio"]          = ( data.BeeKst_pionKaon_mass / data.BeeKst_kaonPion_mass )
    df["diMeson_Kpi_piK_mass_closer_PDG"]       = ( ( abs(data.BeeKst_pionKaon_mass - 891.66) < abs(data.BeeKst_kaonPion_mass - 891.66) ) * data.BeeKst_pionKaon_mass + ( abs(data.BeeKst_pionKaon_mass - 891.66) >= abs(data.BeeKst_kaonPion_mass - 891.66) ) * data.BeeKst_kaonPion_mass )
    df["mass_pionKaon_kaonPion_significance"]   = ( abs(data.BeeKst_kaonPion_mass - data.BeeKst_pionKaon_mass) / np.sqrt(data.BeeKst_kaonPion_mass**2 + data.BeeKst_pionKaon_mass**2) )
    df["mass_positron_trackPlus_kaon"]            = ( vtx_e0_p4 + vtx_m0_K_p4 ).mass
    df["mass_positron_trackMinus_kaon"]           = ( vtx_e0_p4 + vtx_m1_K_p4 ).mass
    df["mass_electron_trackPlus_kaon"]           = ( vtx_e1_p4 + vtx_m0_K_p4 ).mass
    df["mass_electron_trackMinus_kaon"]          = ( vtx_e1_p4 + vtx_m1_K_p4 ).mass
    df["mass_positron_trackPlus_pion"]            = ( vtx_e0_p4 + vtx_m0_pi_p4 ).mass
    df["mass_positron_trackMinus_pion"]           = ( vtx_e0_p4 + vtx_m1_pi_p4 ).mass
    df["mass_electron_trackPlus_pion"]           = ( vtx_e1_p4 + vtx_m0_pi_p4 ).mass
    df["mass_electron_trackMinus_pion"]          = ( vtx_e1_p4 + vtx_m1_pi_p4 ).mass


    # NOTE: K <-> pi doesn't matter here...
    df["dPhi_positron_electron"]       = abs( vtx_e0_p4.deltaphi(vtx_e1_p4) )
    df["dPhi_trackPlus_trackMinus"]    = abs( vtx_m0_K_p4.deltaphi(vtx_m1_pi_p4) )
    df["dPhi_positron_trackPlus"]      = abs( vtx_e0_p4.deltaphi(vtx_m0_K_p4) )
    df["dPhi_positron_trackMinus"]     = abs( vtx_e0_p4.deltaphi(vtx_m1_pi_p4) )
    df["dPhi_electron_trackPlus"]      = abs( vtx_e1_p4.deltaphi(vtx_m0_K_p4) )
    df["dPhi_electron_trackMinus"]     = abs( vtx_e1_p4.deltaphi(vtx_m1_pi_p4) )
    df["dPhi_diElectron_Kstar"]        = abs( vtx_diLepton_p4.deltaphi(vtx_Kpi_p4) )
    df["dPhi_positron_Kstar"]          = abs( vtx_e0_p4.deltaphi(vtx_Kpi_p4) )
    df["dPhi_electron_Kstar"]          = abs( vtx_e1_p4.deltaphi(vtx_Kpi_p4) )
    df["dPhi_diElectron_trackPlus"]    = abs( vtx_diLepton_p4.deltaphi(vtx_m0_K_p4) )
    df["dPhi_diElectron_trackMinus"]   = abs( vtx_diLepton_p4.deltaphi(vtx_m1_pi_p4) )

    df["dR_diElectron_Kstar"]          = vtx_diLepton_p4.deltaR(vtx_Kpi_p4)
    df["dR_positron_Kstar"]            = vtx_e0_p4.deltaR(vtx_Kpi_p4)
    df["dR_electron_Kstar"]            = vtx_e1_p4.deltaR(vtx_Kpi_p4)
    df["dR_diElectron_trackPlus"]      = vtx_diLepton_p4.deltaR(vtx_m0_K_p4)
    df["dR_diElectron_trackMinus"]     = vtx_diLepton_p4.deltaR(vtx_m1_pi_p4)
    df["dR_positron_electron"]         = vtx_e0_p4.deltaR(vtx_e1_p4)
    df["dR_positron_trackPlus"]        = vtx_e0_p4.deltaR(vtx_m0_K_p4)
    df["dR_positron_trackMinus"]       = vtx_e0_p4.deltaR(vtx_m1_pi_p4)
    df["dR_electron_trackPlus"]        = vtx_e1_p4.deltaR(vtx_m0_K_p4)
    df["dR_electron_trackMinus"]       = vtx_e1_p4.deltaR(vtx_m1_pi_p4)
    df["dR_trackPlus_trackMinus"]      = vtx_m0_K_p4.deltaR(vtx_m1_pi_p4)

    df["cos_Theta"]                            = ( vtx_pv2B.dot(vtx_Bd_p4) / np.sqrt( vtx_pv2B.dot(vtx_pv2B) * vtx_Bd_p4.dot(vtx_Bd_p4) ) )
    # See the note above "vtx_Bd_p3_err" definition!!!
    df["cos_Theta_pseudo_err"]                 = cos_Theta_error(vtx_pv2B, vtx_pv2B_err, vtx_Bd_p4, vtx_Bd_p3_err) 
    df["cos_Theta_pseudo_significance"]        = df["cos_Theta"] / df["cos_Theta_pseudo_err"] 

    df["B_chi2"]                               = data.BeeKst_chi2
    df["diElectron_chi2"]                      = data.BeeKst_diElectron_chi2
    df["diMeson_chi2"]                         = data.BeeKst_diMeson_chi2
    df["B_chi2_over_nDoF"]                     = data.BeeKst_chi2_over_nDoF
    df["diElectron_chi2_over_nDoF"]            = data.BeeKst_diElectron_chi2_over_nDoF
    df["diMeson_chi2_over_nDoF"]               = data.BeeKst_diMeson_chi2_over_nDoF
    df["B_pvalue"]                             = stats.chi2.sf(data.BeeKst_chi2,data.BeeKst_nDoF)
    df["diElectron_pvalue"]                    = stats.chi2.sf(data.BeeKst_diElectron_chi2,data.BeeKst_diElectron_nDoF)
    df["diMeson_pvalue"]                       = stats.chi2.sf(data.BeeKst_diMeson_chi2,data.BeeKst_diMeson_nDoF)
    df["B_tau_constM_PVMinA0"]                 = data.BeeKst_B_tau_constM_PVMinA0
    df["Bbar_tau_constM_PVMinA0"]              = data.BeeKst_Bbar_tau_constM_PVMinA0
    df["B_Bbar_tau_closer_PDG"]                = ( ( abs(data.BeeKst_B_tau_constM_PVMinA0 - 1.520) < abs(data.BeeKst_Bbar_tau_constM_PVMinA0 - 1.520) ) * data.BeeKst_B_tau_constM_PVMinA0 + ( abs(data.BeeKst_B_tau_constM_PVMinA0 - 1.520) >= abs(data.BeeKst_Bbar_tau_constM_PVMinA0 - 1.520) ) * data.BeeKst_Bbar_tau_constM_PVMinA0 )
    df["a0_significance"]                      = ( data.BeeKst_a0_minA0 / data.BeeKst_a0_minA0_err )
    df["z0_significance"]                      = ( data.BeeKst_z0_minA0 / data.BeeKst_z0_minA0_err )
    df["Lxy_significance"]                     = ( data.BeeKst_Lxy_minA0 / data.BeeKst_Lxy_minA0_err )
    df["diElectron_angle_alpha"]               = np.pi/2. - vtx_n1.deltaangle(vtx_diLepton_p4)
    df["diMeson_angle_alpha"]                  = np.pi/2. - vtx_n1.deltaangle(vtx_Kpi_p4)
    df["diElectron_angle_beta"]                = vtx_n3_pee.deltaangle(vtx_B2diLepton)
    df["diMeson_angle_beta"]                   = vtx_n3_pmm.deltaangle(vtx_B2diMeson)
    df["diElectron_angle_beta_sym"]            = abs( abs( np.pi/2 - df["diElectron_angle_beta"] ) - np.pi/2 )
    df["diMeson_angle_beta_sym"]               = abs( abs( np.pi/2 - df["diMeson_angle_beta"] ) - np.pi/2 )
    df["diMeson_angle_alpha_sym"]              = df["diMeson_angle_alpha"].abs()
    df["diElectron_angle_alpha_sym"]           = df["diElectron_angle_alpha"].abs()
    df["angle_diMeson_B_diElectron"]           = vtx_B2diMeson.deltaangle(vtx_B2diLepton)   
    df["dR_diMeson_B_diElectron"]              = vtx_B2diMeson_v4.deltaR(vtx_B2diLepton_v4)
    df["angle_vtx_plane_ee_plane"]             = vtx_n1.deltaangle(vtx_n2_ee)
    df["angle_vtx_plane_mm_plane"]             = vtx_n1.deltaangle(vtx_n2_mm)
    df["angle_pp_plane_ee_plane"]              = vtx_n1_pp.deltaangle(vtx_n2_ee)
    df["angle_pp_plane_mm_plane"]              = vtx_n1_pp.deltaangle(vtx_n2_mm)
    df["angle_ee_plane_mm_plane"]              = vtx_n2_ee.deltaangle(vtx_n2_mm)
    df["d_B_diMeson_significance"]             = distance_significance(vtx_Bd_vtx, vtx_Bd_vtx_err, vtx_diMeson_vtx, vtx_diMeson_vtx_err) 
    df["d_B_diElectron_significance"]          = distance_significance(vtx_Bd_vtx, vtx_Bd_vtx_err, vtx_diLepton_vtx, vtx_diLepton_vtx_err)
    df["d_diMeson_diElectron_significance"]    = distance_significance(vtx_diMeson_vtx, vtx_diMeson_vtx_err, vtx_diLepton_vtx, vtx_diLepton_vtx_err)
    df["positron_pvalue"]                      = stats.chi2.sf(data.BeeKst_electron0_chi2,data.BeeKst_electron0_nDoF)
    df["electron_pvalue"]                      = stats.chi2.sf(data.BeeKst_electron1_chi2,data.BeeKst_electron1_nDoF)
    df["trackPlus_pvalue"]                     = stats.chi2.sf(data.BeeKst_meson0_chi2,data.BeeKst_meson0_nDoF)
    df["trackMinus_pvalue"]                    = stats.chi2.sf(data.BeeKst_meson1_chi2,data.BeeKst_meson1_nDoF)
    df["positron_chi2"]                        = data.BeeKst_electron0_chi2
    df["electron_chi2"]                        = data.BeeKst_electron1_chi2
    df["trackPlus_chi2"]                       = data.BeeKst_meson0_chi2
    df["trackMinus_chi2"]                      = data.BeeKst_meson1_chi2
    df["positron_nDoF"]                        = data.BeeKst_electron0_nDoF
    df["electron_nDoF"]                        = data.BeeKst_electron1_nDoF
    df["trackPlus_nDoF"]                       = data.BeeKst_meson0_nDoF
    df["trackMinus_nDoF"]                      = data.BeeKst_meson1_nDoF
    df["positron_chi2_over_nDoF"]              = ( data.BeeKst_electron0_chi2 / data.BeeKst_electron0_nDoF )
    df["electron_chi2_over_nDoF"]              = ( data.BeeKst_electron1_chi2 / data.BeeKst_electron1_nDoF )
    df["trackPlus_chi2_over_nDoF"]             = ( data.BeeKst_meson0_chi2 / data.BeeKst_meson0_nDoF )
    df["trackMinus_chi2_over_nDoF"]            = ( data.BeeKst_meson1_chi2 / data.BeeKst_meson1_nDoF )


    df["info_event_number"]      = ( data.BeeKst_B_mass * 0 + data.event_number ).astype(int)
    df["info_sample"]         = str(mc_sample)
    df["info_sample"] = df["info_sample"].astype(pd.StringDtype())
    df["info_sample_event_number"]             = (df["info_sample"].astype(str) + "_" + df["info_event_number"].astype(str))
    df["info_sample_event_number"] = df["info_sample_event_number"].astype(pd.StringDtype())
    df["info_low_q2"] =                         ((data.BeeKst_diElectron_mass**2 <= 6000000) & (1100 <= data.BeeKst_diElectron_mass**2))*1
    df["info_high_q2"] =                         ((data.BeeKst_diElectron_mass**2 <= 11000000) & (6000000 <= data.BeeKst_diElectron_mass**2))*1
    
    df["track_multiplicity_after_cuts"]        = ( data.BeeKst_B_mass * 0 + track_multiplicity ).astype(int)
    
    df["electron_multiplicity_after_cuts"]     = ( data.BeeKst_B_mass * 0 + electron_multiplicity ).astype(int)
        
    df["info_diElectron_gsf_id_isOK"]                    = data.BeeKst_diElectron_gsf_id_isOK*1
    df["positron_ptvarcone30_TightTTVA_pt1000"]          = data.BeeKst_electron0_ptvarcone30_TightTTVA_pt1000
    df["positron_ptvarcone20_TightTTVA_pt1000"]          = data.BeeKst_electron0_ptvarcone20_TightTTVA_pt1000
    df["positron_ptvarcone30_TightTTVA_pt500"]           = data.BeeKst_electron0_ptvarcone30_TightTTVA_pt500
    df["positron_ptvarcone40_TightTTVALooseCone_pt1000"] = data.BeeKst_electron0_ptvarcone40_TightTTVALooseCone_pt1000
    df["positron_ptvarcone30_TightTTVALooseCone_pt1000"] = data.BeeKst_electron0_ptvarcone30_TightTTVALooseCone_pt1000
    df["positron_ptcone20_TightTTVALooseCone_pt1000"]    = data.BeeKst_electron0_ptcone20_TightTTVALooseCone_pt1000
    df["positron_ptvarcone20_TightTTVALooseCone_pt1000"] = data.BeeKst_electron0_ptvarcone20_TightTTVALooseCone_pt1000
    df["positron_ptvarcone30_TightTTVALooseCone_pt500"]  = data.BeeKst_electron0_ptvarcone30_TightTTVALooseCone_pt500
    df["positron_ptcone20_TightTTVALooseCone_pt500"]     = data.BeeKst_electron0_ptcone20_TightTTVALooseCone_pt500
    df["positron_ECIDSResult"]                           = data.BeeKst_electron0_DFCommonElectronsECIDSResult
    df["positron_AddAmbiguity"]                          = data.BeeKst_electron0_DFCommonAddAmbiguity
    df["positron_OQ"]                                    = data.BeeKst_electron0_OQ
    df["positron_LHVeryLooseIsEMValue"]                  = data.BeeKst_electron0_DFCommonElectronsLHVeryLooseIsEMValue
    df["positron_LHLooseIsEMValue"]                      = data.BeeKst_electron0_DFCommonElectronsLHLooseIsEMValue
    df["positron_LHLooseBLIsEMValue"]                    = data.BeeKst_electron0_DFCommonElectronsLHLooseBLIsEMValue
    df["positron_LHMediumIsEMValue"]                     = data.BeeKst_electron0_DFCommonElectronsLHMediumIsEMValue
    df["positron_LHTightIsEMValue"]                      = data.BeeKst_electron0_DFCommonElectronsLHTightIsEMValue
    df["positron_author"]                                = data.BeeKst_electron0_author
    df["positron_LHVeryLoose"]                           = data.BeeKst_electron0_DFCommonElectronsLHVeryLoose
    df["positron_LHLoose"]                               = data.BeeKst_electron0_DFCommonElectronsLHLoose
    df["positron_LHLooseBL"]                             = data.BeeKst_electron0_DFCommonElectronsLHLooseBL
    df["positron_LHMedium"]                              = data.BeeKst_electron0_DFCommonElectronsLHMedium
    df["positron_LHTight"]                               = data.BeeKst_electron0_DFCommonElectronsLHTight
    df["positron_ECIDS"]                                 = data.BeeKst_electron0_DFCommonElectronsECIDS
    df["positron_CrackVetoCleaning"]                     = data.BeeKst_electron0_DFCommonCrackVetoCleaning
    df["positron_LHVeryLoosenod0"]                       = data.BeeKst_electron0_DFCommonElectronsLHVeryLoosenod0
    df["electron_ptvarcone30_TightTTVA_pt1000"]          = data.BeeKst_electron1_ptvarcone30_TightTTVA_pt1000
    df["electron_ptvarcone20_TightTTVA_pt1000"]          = data.BeeKst_electron1_ptvarcone20_TightTTVA_pt1000
    df["electron_ptvarcone30_TightTTVA_pt500"]           = data.BeeKst_electron1_ptvarcone30_TightTTVA_pt500
    df["electron_ptvarcone40_TightTTVALooseCone_pt1000"] = data.BeeKst_electron1_ptvarcone40_TightTTVALooseCone_pt1000
    df["electron_ptvarcone30_TightTTVALooseCone_pt1000"] = data.BeeKst_electron1_ptvarcone30_TightTTVALooseCone_pt1000
    df["electron_ptcone20_TightTTVALooseCone_pt1000"]    = data.BeeKst_electron1_ptcone20_TightTTVALooseCone_pt1000
    df["electron_ptvarcone20_TightTTVALooseCone_pt1000"] = data.BeeKst_electron1_ptvarcone20_TightTTVALooseCone_pt1000
    df["electron_ptvarcone30_TightTTVALooseCone_pt500"]  = data.BeeKst_electron1_ptvarcone30_TightTTVALooseCone_pt500
    df["electron_ptcone20_TightTTVALooseCone_pt500"]     = data.BeeKst_electron1_ptcone20_TightTTVALooseCone_pt500
    df["electron_ECIDSResult"]                           = data.BeeKst_electron1_DFCommonElectronsECIDSResult
    df["electron_AddAmbiguity"]                          = data.BeeKst_electron1_DFCommonAddAmbiguity
    df["electron_OQ"]                                    = data.BeeKst_electron1_OQ
    df["electron_LHVeryLooseIsEMValue"]                  = data.BeeKst_electron1_DFCommonElectronsLHVeryLooseIsEMValue
    df["electron_LHLooseIsEMValue"]                      = data.BeeKst_electron1_DFCommonElectronsLHLooseIsEMValue
    df["electron_LHLooseBLIsEMValue"]                    = data.BeeKst_electron1_DFCommonElectronsLHLooseBLIsEMValue
    df["electron_LHMediumIsEMValue"]                     = data.BeeKst_electron1_DFCommonElectronsLHMediumIsEMValue
    df["electron_LHTightIsEMValue"]                      = data.BeeKst_electron1_DFCommonElectronsLHTightIsEMValue
    df["electron_author"]                                = data.BeeKst_electron1_author
    df["electron_LHVeryLoose"]                           = data.BeeKst_electron1_DFCommonElectronsLHVeryLoose
    df["electron_LHLoose"]                               = data.BeeKst_electron1_DFCommonElectronsLHLoose
    df["electron_LHLooseBL"]                             = data.BeeKst_electron1_DFCommonElectronsLHLooseBL
    df["electron_LHMedium"]                              = data.BeeKst_electron1_DFCommonElectronsLHMedium
    df["electron_LHTight"]                               = data.BeeKst_electron1_DFCommonElectronsLHTight
    df["electron_ECIDS"]                                 = data.BeeKst_electron1_DFCommonElectronsECIDS
    df["electron_CrackVetoCleaning"]                     = data.BeeKst_electron1_DFCommonCrackVetoCleaning
    df["electron_LHVeryLoosenod0"]                       = data.BeeKst_electron1_DFCommonElectronsLHVeryLoosenod0

    df["positron_charge"]    = data.BeeKst_electron0_charge
    df["electron_charge"]    = data.BeeKst_electron1_charge
    df["trackPlus_charge"]   = data.BeeKst_meson0_charge
    df["trackMinus_charge"]  = data.BeeKst_meson1_charge

    df["diElectron_iso_m00"] = data.BeeKst_diElectron_iso_m00
    df["diElectron_iso_m05"] = data.BeeKst_diElectron_iso_m05
    df["diElectron_iso_m10"] = data.BeeKst_diElectron_iso_m10
    df["diElectron_iso_m15"] = data.BeeKst_diElectron_iso_m15
    df["diElectron_iso_m20"] = data.BeeKst_diElectron_iso_m20
    df["diElectron_iso_m25"] = data.BeeKst_diElectron_iso_m25
    df["diElectron_iso_m30"] = data.BeeKst_diElectron_iso_m30

    df["positron_iso_c05"]   = data.BeeKst_electron0_iso_c05
    df["positron_iso_c10"]   = data.BeeKst_electron0_iso_c10
    df["positron_iso_c15"]   = data.BeeKst_electron0_iso_c15
    df["positron_iso_c20"]   = data.BeeKst_electron0_iso_c20
    df["positron_iso_c25"]   = data.BeeKst_electron0_iso_c25
    df["positron_iso_c30"]   = data.BeeKst_electron0_iso_c30
    df["positron_iso_c35"]   = data.BeeKst_electron0_iso_c35
    df["positron_iso_c40"]   = data.BeeKst_electron0_iso_c40

    df["electron_iso_c05"]   = data.BeeKst_electron1_iso_c05
    df["electron_iso_c10"]   = data.BeeKst_electron1_iso_c10
    df["electron_iso_c15"]   = data.BeeKst_electron1_iso_c15
    df["electron_iso_c20"]   = data.BeeKst_electron1_iso_c20
    df["electron_iso_c25"]   = data.BeeKst_electron1_iso_c25
    df["electron_iso_c30"]   = data.BeeKst_electron1_iso_c30
    df["electron_iso_c35"]   = data.BeeKst_electron1_iso_c35
    df["electron_iso_c40"]   = data.BeeKst_electron1_iso_c40


    df["trackPlus_iso_c05"]  = data.BeeKst_meson0_iso_c05
    df["trackPlus_iso_c10"]  = data.BeeKst_meson0_iso_c10
    df["trackPlus_iso_c15"]  = data.BeeKst_meson0_iso_c15
    df["trackPlus_iso_c20"]  = data.BeeKst_meson0_iso_c20
    df["trackPlus_iso_c25"]  = data.BeeKst_meson0_iso_c25
    df["trackPlus_iso_c30"]  = data.BeeKst_meson0_iso_c30
    df["trackPlus_iso_c35"]  = data.BeeKst_meson0_iso_c35
    df["trackPlus_iso_c40"]  = data.BeeKst_meson0_iso_c40

    df["trackMinus_iso_c05"] = data.BeeKst_meson1_iso_c05
    df["trackMinus_iso_c10"] = data.BeeKst_meson1_iso_c10
    df["trackMinus_iso_c15"] = data.BeeKst_meson1_iso_c15
    df["trackMinus_iso_c20"] = data.BeeKst_meson1_iso_c20
    df["trackMinus_iso_c25"] = data.BeeKst_meson1_iso_c25
    df["trackMinus_iso_c30"] = data.BeeKst_meson1_iso_c30
    df["trackMinus_iso_c35"] = data.BeeKst_meson1_iso_c35
    df["trackMinus_iso_c40"] = data.BeeKst_meson1_iso_c40

    df["electron0_p"]  = (df["positron_pT"] > df["electron_pT"]) * df["positron_p"]   + (df["positron_pT"] <= df["electron_pT"]) * df["electron_p"]
    df["electron1_p"]  = (df["positron_pT"] > df["electron_pT"]) * df["electron_p"]   + (df["positron_pT"] <= df["electron_pT"]) * df["positron_p"]
    df["electron0_pz"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_pz"]  + (df["positron_pT"] <= df["electron_pT"]) * df["electron_pz"]
    df["electron1_pz"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_pz"]  + (df["positron_pT"] <= df["electron_pT"]) * df["positron_pz"]
    df["electron0_pT"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_pT"]  + (df["positron_pT"] <= df["electron_pT"]) * df["electron_pT"]
    df["electron1_pT"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_pT"]  + (df["positron_pT"] <= df["electron_pT"]) * df["positron_pT"]
    df["electron0_eta"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_eta"] + (df["positron_pT"] <= df["electron_pT"]) * df["electron_eta"]
    df["electron1_eta"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_eta"] + (df["positron_pT"] <= df["electron_pT"]) * df["positron_eta"]
    df["electron0_phi"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_phi"] + (df["positron_pT"] <= df["electron_pT"]) * df["electron_phi"]
    df["electron1_phi"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_phi"] + (df["positron_pT"] <= df["electron_pT"]) * df["positron_phi"]

    df["track0_p"]   = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_p"]    + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_p"]
    df["track1_p"]   = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_p"]   + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_p"]
    df["track0_pz"]  = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_pz"]   + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_pz"]
    df["track1_pz"]  = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_pz"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_pz"]
    df["track0_pT"]  = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_pT"]   + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_pT"]
    df["track1_pT"]  = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_pT"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_pT"]
    df["track0_eta"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_eta"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_eta"]
    df["track1_eta"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_eta"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_eta"]
    df["track0_phi"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_phi"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_phi"]
    df["track1_phi"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_phi"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_phi"]

    df["kaon_pT"]  = ( abs(df["diMeson_piK_mass"] - 891.66) < abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackMinus_pT"]  + ( abs(df["diMeson_piK_mass"] - 891.66) >= abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackPlus_pT"]
    df["pion_pT"]  = ( abs(df["diMeson_piK_mass"] - 891.66) < abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackPlus_pT"]   + ( abs(df["diMeson_piK_mass"] - 891.66) >= abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackMinus_pT"]
    df["kaon_eta"] = ( abs(df["diMeson_piK_mass"] - 891.66) < abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackMinus_eta"] + ( abs(df["diMeson_piK_mass"] - 891.66) >= abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackPlus_eta"]
    df["pion_eta"] = ( abs(df["diMeson_piK_mass"] - 891.66) < abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackPlus_eta"]  + ( abs(df["diMeson_piK_mass"] - 891.66) >= abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackMinus_eta"]
    df["kaon_phi"] = ( abs(df["diMeson_piK_mass"] - 891.66) < abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackMinus_phi"] + ( abs(df["diMeson_piK_mass"] - 891.66) >= abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackPlus_phi"]
    df["pion_phi"] = ( abs(df["diMeson_piK_mass"] - 891.66) < abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackPlus_phi"]  + ( abs(df["diMeson_piK_mass"] - 891.66) >= abs(df["diMeson_Kpi_mass"] - 891.66) ) * df["trackMinus_phi"]

    df["electron0_iso_c05"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_iso_c05"] + (df["positron_pT"] <= df["electron_pT"]) * df["electron_iso_c05"]
    df["electron1_iso_c05"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_iso_c05"] + (df["positron_pT"] <= df["electron_pT"]) * df["positron_iso_c05"]
    df["electron0_iso_c10"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_iso_c10"] + (df["positron_pT"] <= df["electron_pT"]) * df["electron_iso_c10"]
    df["electron1_iso_c10"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_iso_c10"] + (df["positron_pT"] <= df["electron_pT"]) * df["positron_iso_c10"]
    df["electron0_iso_c15"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_iso_c15"] + (df["positron_pT"] <= df["electron_pT"]) * df["electron_iso_c15"]
    df["electron1_iso_c15"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_iso_c15"] + (df["positron_pT"] <= df["electron_pT"]) * df["positron_iso_c15"]
    df["electron0_iso_c20"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_iso_c20"] + (df["positron_pT"] <= df["electron_pT"]) * df["electron_iso_c20"]
    df["electron1_iso_c20"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_iso_c20"] + (df["positron_pT"] <= df["electron_pT"]) * df["positron_iso_c20"]
    df["electron0_iso_c25"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_iso_c25"] + (df["positron_pT"] <= df["electron_pT"]) * df["electron_iso_c25"]
    df["electron1_iso_c25"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_iso_c25"] + (df["positron_pT"] <= df["electron_pT"]) * df["positron_iso_c25"]
    df["electron0_iso_c30"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_iso_c30"] + (df["positron_pT"] <= df["electron_pT"]) * df["electron_iso_c30"]
    df["electron1_iso_c30"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_iso_c30"] + (df["positron_pT"] <= df["electron_pT"]) * df["positron_iso_c30"]
    df["electron0_iso_c35"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_iso_c35"] + (df["positron_pT"] <= df["electron_pT"]) * df["electron_iso_c35"]
    df["electron1_iso_c35"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_iso_c35"] + (df["positron_pT"] <= df["electron_pT"]) * df["positron_iso_c35"]
    df["electron0_iso_c40"] = (df["positron_pT"] > df["electron_pT"]) * df["positron_iso_c40"] + (df["positron_pT"] <= df["electron_pT"]) * df["electron_iso_c40"]
    df["electron1_iso_c40"] = (df["positron_pT"] > df["electron_pT"]) * df["electron_iso_c40"] + (df["positron_pT"] <= df["electron_pT"]) * df["positron_iso_c40"]


    df["track0_iso_c05"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c05"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c05"]
    df["track1_iso_c05"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c05"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c05"]
    df["track0_iso_c10"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c10"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c10"]
    df["track1_iso_c10"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c10"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c10"]
    df["track0_iso_c15"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c15"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c15"]
    df["track1_iso_c15"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c15"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c15"]
    df["track0_iso_c20"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c20"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c20"]
    df["track1_iso_c20"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c20"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c20"]
    df["track0_iso_c25"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c25"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c25"]
    df["track1_iso_c25"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c25"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c25"]
    df["track0_iso_c30"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c30"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c30"]
    df["track1_iso_c30"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c30"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c30"]
    df["track0_iso_c35"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c35"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c35"]
    df["track1_iso_c35"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c35"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c35"]
    df["track0_iso_c40"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c40"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c40"]
    df["track1_iso_c40"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c40"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c40"]


    df["diMeson_KK_mass"]               = ( vtx_m0_K_p4  + vtx_m1_K_p4  ).mass
    df["diMeson_pipi_mass"]             = ( vtx_m0_pi_p4 + vtx_m1_pi_p4 ).mass

    df["diMeson_Kpi_mass_pT"]           = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["diMeson_Kpi_mass"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["diMeson_piK_mass"]
    df["diMeson_piK_mass_pT"]           = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["diMeson_piK_mass"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["diMeson_Kpi_mass"]
    df["diMeson_Kpi_piK_mass_avg"]      = ( df["diMeson_Kpi_mass"] + df["diMeson_piK_mass"] ) / 2
    df["diMeson_Kpi_piK_mass_diff"]     = df["diMeson_Kpi_mass"]    - df["diMeson_piK_mass"]
    df["diMeson_Kpi_piK_mass_pT_diff"]  = df["diMeson_Kpi_mass_pT"] - df["diMeson_piK_mass_pT"]
    df["diMeson_Kpi_piK_mass_ratio"]    = df["diMeson_Kpi_mass"]    / df["diMeson_piK_mass"]
    df["diMeson_Kpi_piK_mass_pT_ratio"] = df["diMeson_Kpi_mass_pT"] / df["diMeson_piK_mass_pT"]
    df["Lxy_significance_over_B_chi2"]  = df["Lxy_significance"] / df["B_chi2"]

    # NOTE: calibration
    df["trackPlus_dEdx"]       = data.BeeKst_meson0_pixeldEdx
    df["trackMinus_dEdx"]      = data.BeeKst_meson1_pixeldEdx

    df["track0_dEdx"]          = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_dEdx"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_dEdx"]
    df["track1_dEdx"]          = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_dEdx"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_dEdx"]

    df["tracks_dEdx_sum"]      = df["trackPlus_dEdx"] + df["trackMinus_dEdx"]
    df["tracks_dEdx_diff"]     = df["trackPlus_dEdx"] - df["trackMinus_dEdx"]
    df["tracks_dEdx_ratio"]    = df["trackPlus_dEdx"] / df["trackMinus_dEdx"]
    df["tracks_dEdx_pT_diff"]  = df["track0_dEdx"] - df["track1_dEdx"]
    df["tracks_dEdx_pT_ratio"] = df["track0_dEdx"] / df["track1_dEdx"]

    df["info_sample_weight"]         = sample_weight
    df["info_candidate_count"]       = ( data.BeeKst_B_mass * 0 + np.count_nonzero(data.BeeKst_B_mass )).astype(int)
    df["info_candidate_count_orig"]  = ( data.BeeKst_B_mass * 0 + np.count_nonzero(data.BeeKst_B_mass )).astype(int)
    df["info_candidate_weight"]      = ( data.BeeKst_B_mass * 0 + data.weight )
    df["info_weight"]                = ( data.BeeKst_B_mass * 0 + sample_weight * data.weight )
    df["info_truth_matching"] = vtx_truth.values * 1

    df["info_full_selection"]           = 1*(df["trackPlus_pT"]>1000) & (df["trackMinus_pT"]>1000) & (df["B_chi2_over_nDoF"]<2) & (((df["B_tau_constM_PVMinA0"]>0.2) & (abs(df["diMeson_Kpi_mass"]-891.66)<50) & (df["B_mass"]>4700) & (df["B_mass"]<5700)) | ((df["Bbar_tau_constM_PVMinA0"]>0.2) & (abs(df["diMeson_piK_mass"]-891.66)<50) & (df["Bbar_mass"]>4700) & (df["Bbar_mass"]<5700)))
    df["info_full_selection"] = df["info_full_selection"].astype(int)
    df["info_full_selection_no_B_mass"] = 1*(df["trackPlus_pT"]>1000) & (df["trackMinus_pT"]>1000) & (df["B_chi2_over_nDoF"]<2) & (((df["B_tau_constM_PVMinA0"]>0.2) & (abs(df["diMeson_Kpi_mass"]-891.66)<50)) | ((df["Bbar_tau_constM_PVMinA0"]>0.2) & (abs(df["diMeson_piK_mass"]-891.66)<50)))
    df["info_full_selection_no_B_mass"] = df["info_full_selection_no_B_mass"].astype(int)

    data = pd.concat([data, df], axis=1)

    remove_null_errors(data)
    remove_unfilled_variables(data)
    remove_duplicated_candidates(data)

    return data

class Multiprocessor():

    def __init__(self):
        self.processes = []
        self.queue = Queue()

    @staticmethod
    def _wrapper(func, queue, args, kwargs):
        ret = func(*args, **kwargs)
        queue.put(ret)

    def run(self, func:list, *args, **kwargs):
        args21 = [func[0], self.queue, args[0][:2], kwargs]
        args22 = [func[1], self.queue, args[0][1:3], kwargs]
        args23 = [func[2], self.queue, args[0][2:4], kwargs]
        args24 = [func[3], self.queue, args[0][3:5], kwargs]
        p1 = Process(target=self._wrapper, args=args21)
        p2 = Process(target=self._wrapper, args=args22)
        p3 = Process(target=self._wrapper, args=args23)
        p4 = Process(target=self._wrapper, args=args24)


        self.processes.append(p1)
        self.processes.append(p2)
        self.processes.append(p3)
        self.processes.append(p4)
        p1.start()
        p2.start()
        p3.start()
        p4.start()


    def wait(self):
        rets = []
        for p in self.processes:
            ret = self.queue.get()
            rets.append(ret)
        for p in self.processes:
            p.join()
        return rets


def selections():
    selection_main = '((BeeKst_diElectron_gsf_id_isOK == 1) & (BeeKst_electron0_charge * BeeKst_electron1_charge < 0) & (BeeKst_electron1_iso_c40 < 100000) & (BeeKst_electron0_iso_c40 < 100000) )'

    B_mass_Kstar_mass_closer__string   =  '((abs(BeeKst_kaonPion_mass-891.66)<abs(BeeKst_pionKaon_mass-891.66))*BeeKst_B_mass+(abs(BeeKst_kaonPion_mass-891.66)>=abs(BeeKst_pionKaon_mass-891.66))*BeeKst_Bbar_mass)'

    selection_nn1_sb =  f"( ((({B_mass_Kstar_mass_closer__string}) <= 4000) | ( ({B_mass_Kstar_mass_closer__string}) >= 5700) ) & (BeeKst_chi2 < 20) )"
    selection_nn1_sr = f"( (({B_mass_Kstar_mass_closer__string}) > 4000) & (({B_mass_Kstar_mass_closer__string}) < 5700) & (BeeKst_chi2 < 20) )"

    MC_sample = "((BeeKst_isTrueResBd + BeeKst_isTrueNonresBd + BeeKst_isTrueResBdbar + BeeKst_isTrueNonresBdbar) == 1)"


    selection_nn2_sb = '(BeeKst_meson0_charge * BeeKst_meson1_charge > 0)'
    selection_nn2_sr = '(BeeKst_meson0_charge * BeeKst_meson1_charge < 0)'

    selection_q2low =  '( (BeeKst_diElectron_mass*BeeKst_diElectron_mass > 1.1e6) & (BeeKst_diElectron_mass*BeeKst_diElectron_mass <= 6e6) )'
    selection_q2high = '( (BeeKst_diElectron_mass*BeeKst_diElectron_mass > 6e6) & (BeeKst_diElectron_mass*BeeKst_diElectron_mass <= 11e6) )'
    selection_q2lcr =  '(BeeKst_diElectron_mass*BeeKst_diElectron_mass <= 1.1e6)'
    selection_q2hcr =  '(BeeKst_diElectron_mass*BeeKst_diElectron_mass > 11e6)'

    selection_usr =     f"& ({selection_main} & {selection_q2low} & {MC_sample} &  {selection_nn1_sr} & {selection_nn2_sr})"
    selection_nn1trb = f"& ({selection_main} & {selection_q2low} &  {selection_nn1_sb} & {selection_nn2_sr})"
    selection_nn2trb = f"& ({selection_main} & {selection_q2low} & {selection_nn1_sr} & {selection_nn2_sb})"

    return selection_usr, selection_nn1trb, selection_nn2trb

# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------



def Signal():
    PATH_file_name= 'MC_signal_train.ftr'
    print('Starting')

    Files_list = [
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300590_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300590_part_02.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300590_part_03.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300590_part_04.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300591_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300591_part_02.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300591_part_03.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300592_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300593_part_01.root',
    ]


    """
    additional_filter = "& ((BeeKst_electron0_charge * BeeKst_electron1_charge) <0)"
    additional_filter += "& ((BeeKst_meson0_charge * BeeKst_meson1_charge) < 0)"
    additional_filter += "& (BeeKst_chi2 < 20)"
    additional_filter += "& (BeeKst_diElectron_mass * BeeKst_diElectron_mass <= 6000000)" #bound of low q^2
    additional_filter += "& (1100 <= BeeKst_diElectron_mass * BeeKst_diElectron_mass)" #bound of low q^2
    additional_filter += "& ((BeeKst_isTrueResBd + BeeKst_isTrueNonresBd + BeeKst_isTrueResBdbar + BeeKst_isTrueNonresBdbar) > 0)"
    additional_filter += "& (((4000 < BeeKst_B_mass) & (BeeKst_B_mass < 5700))"
    additional_filter += "& ((4000 < BeeKst_Bbar_mass) & (BeeKst_Bbar_mass < 5700)))"
    """
    USR_selection ,_,_= selections()
    
    timer_all = Timer()
    timer_all.start()

    df = pd.DataFrame()
    for root_PATH in Files_list:
        N_entries,process_array = prep_multiprocess(root_PATH=root_PATH, N_process=4,max_entries=None)
        print(f"check array indexes {process_array}")
        timer = Timer()
        timer.start()

        def multi_input(start,end):
            vtx_BeeKst, vtx_GSF, vtx_InDet = root_file_opener(root_PATH, additional_filter = USR_selection, start_entry =start , stop_entry=end)
            if len(vtx_BeeKst)>0:
                data = add_features(data=vtx_BeeKst, data_GSF = vtx_GSF, data_InDet=vtx_InDet)
            else:
                data = vtx_BeeKst
            return data

        
        def run_MP():
            mp = Multiprocessor()
            num_proc = 4
            mp.run([
                multi_input,multi_input,multi_input,multi_input],process_array)
            ret = mp.wait() # get all results
            print(f"Shape of each sub-process: Process 1-4: {' '.join([' | ' + str(i.shape)+' | ' for i in ret])}")

            df = pd.DataFrame()
            for i in ret:
                df = pd.concat([df,i],axis=0)
            
            df = df.sort_index()

            return df

        data = run_MP()
        print()
        print(f"{data.shape} is the shape of file {Files_list.index(root_PATH)} out of {len(Files_list)}")
        df = pd.concat([df,data],axis=0)
        print()
        print(f"Accumulated shape: {df.shape} ")        
        print()
        print(f"Time it took to do file {Files_list.index(root_PATH)+1} out of {len(Files_list)} files")
        timer.stop()
        print()
    
    save_to_feater(df, PATH=PATH_file_name)
    print(f"Total script time:")
    timer_all.stop()

# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------


def Data_SB():
    PATH_file_name= 'Data_SB_train.ftr'

    print('Starting')
    files_folder = '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/'

    Files_list_K = sorted([files_folder + x for x in os.listdir(files_folder) if 'ntuple-data18_13TeV_periodK' in x ])


    Files_list = Files_list_K

    """
    additional_filter = "& ((BeeKst_electron0_charge * BeeKst_electron1_charge) <0)"
    additional_filter += "& ((BeeKst_meson0_charge * BeeKst_meson1_charge) < 0)"
    additional_filter += "& (BeeKst_chi2 < 20)"
    additional_filter += "& (BeeKst_diElectron_mass * BeeKst_diElectron_mass <= 6000000)" #bound of low q^2
    additional_filter += "& (1100 <= BeeKst_diElectron_mass * BeeKst_diElectron_mass)" #bound of low q^2
    additional_filter += "& ((BeeKst_isTrueResBd + BeeKst_isTrueNonresBd + BeeKst_isTrueResBdbar + BeeKst_isTrueNonresBdbar) > 0)"
    additional_filter += "& (((4000 < BeeKst_B_mass) & (BeeKst_B_mass < 5700))"
    additional_filter += "& ((4000 < BeeKst_Bbar_mass) & (BeeKst_Bbar_mass < 5700)))"
    """
    
    _, NN1_selction,_= selections()

    timer_all = Timer()
    timer_all.start()

    df = pd.DataFrame()
    for root_PATH in Files_list:
        N_entries,process_array = prep_multiprocess(root_PATH=root_PATH, N_process=4,max_entries=None)
        print(f"check array indexes {process_array}")
        timer = Timer()
        timer.start()

        def multi_input(start,end):
            vtx_BeeKst, vtx_GSF, vtx_InDet = root_file_opener(root_PATH, additional_filter = NN1_selction, start_entry =start , stop_entry=end)
            if len(vtx_BeeKst)>0:
                data = add_features(data=vtx_BeeKst, data_GSF = vtx_GSF, data_InDet=vtx_InDet)

                # select best 6 Lxy_sig/chi2
                data=data.sort_values('Lxy_significance_over_B_chi2').groupby(level=0).tail(6).sort_index()
            else:
                data = vtx_BeeKst
            return data

        
        def run_MP():
            mp = Multiprocessor()
            num_proc = 4
            mp.run([
                multi_input,multi_input,multi_input,multi_input],process_array)
            ret = mp.wait() # get all results
            print(f"Shape of each sub-process: Process 1-4: {' '.join([' | ' + str(i.shape)+' | ' for i in ret])}")

            df = pd.DataFrame()
            for i in ret:
                df = pd.concat([df,i],axis=0)
            
            df = df.sort_index()

            return df

        data = run_MP()
        print()
        print(f"{data.shape} is the shape of file {Files_list.index(root_PATH)} out of {len(Files_list)}")
        df = pd.concat([df,data],axis=0)
        print()
        print(f"Accumulated shape: {df.shape} ")        
        print()
        print(f"Time it took to do file {Files_list.index(root_PATH)+1} out of {len(Files_list)} files")
        timer.stop()
        print()
    
    save_to_feater(df, PATH=PATH_file_name)
    print(f"Total script time:")
    timer_all.stop()

# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------


def Data_SR():
    PATH_file_name= 'Data_SR_train.ftr'

    print('Starting')
    files_folder = '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/'

    Files_list_K = sorted([files_folder + x for x in os.listdir(files_folder) if 'ntuple-data18_13TeV_periodK' in x ])


    Files_list = Files_list_K

    """
    additional_filter = "& ((BeeKst_electron0_charge * BeeKst_electron1_charge) <0)"
    additional_filter += "& ((BeeKst_meson0_charge * BeeKst_meson1_charge) < 0)"
    additional_filter += "& (BeeKst_chi2 < 20)"
    additional_filter += "& (BeeKst_diElectron_mass * BeeKst_diElectron_mass <= 6000000)" #bound of low q^2
    additional_filter += "& (1100 <= BeeKst_diElectron_mass * BeeKst_diElectron_mass)" #bound of low q^2
    additional_filter += "& ((BeeKst_isTrueResBd + BeeKst_isTrueNonresBd + BeeKst_isTrueResBdbar + BeeKst_isTrueNonresBdbar) > 0)"
    additional_filter += "& (((4000 < BeeKst_B_mass) & (BeeKst_B_mass < 5700))"
    additional_filter += "& ((4000 < BeeKst_Bbar_mass) & (BeeKst_Bbar_mass < 5700)))"
    """
    
    _,_, NN2_selction= selections()

    timer_all = Timer()
    timer_all.start()

    df = pd.DataFrame()
    for root_PATH in Files_list:
        N_entries,process_array = prep_multiprocess(root_PATH=root_PATH, N_process=4,max_entries=None)
        print(f"check array indexes {process_array}")
        timer = Timer()
        timer.start()

        def multi_input(start,end):
            vtx_BeeKst, vtx_GSF, vtx_InDet = root_file_opener(root_PATH, additional_filter = NN2_selction, start_entry =start , stop_entry=end)
            if len(vtx_BeeKst)>0:
                data = add_features(data=vtx_BeeKst, data_GSF = vtx_GSF, data_InDet=vtx_InDet)

                # select best 6 Lxy_sig/chi2
                data=data.sort_values('Lxy_significance_over_B_chi2').groupby(level=0).tail(6).sort_index()
            else:
                data = vtx_BeeKst
            return data

        
        def run_MP():
            mp = Multiprocessor()
            num_proc = 4
            mp.run([
                multi_input,multi_input,multi_input,multi_input],process_array)
            ret = mp.wait() # get all results
            print(f"Shape of each sub-process: Process 1-4: {' '.join([' | ' + str(i.shape)+' | ' for i in ret])}")

            df = pd.DataFrame()
            for i in ret:
                df = pd.concat([df,i],axis=0)
            
            df = df.sort_index()

            return df

        data = run_MP()
        print()
        print(f"{data.shape} is the shape of file {Files_list.index(root_PATH)} out of {len(Files_list)}")
        df = pd.concat([df,data],axis=0)
        print()
        print(f"Accumulated shape: {df.shape} ")        
        print()
        print(f"Time it took to do file {Files_list.index(root_PATH)+1} out of {len(Files_list)} files")
        timer.stop()
        print()
    
    save_to_feater(df, PATH=PATH_file_name)
    print(f"Total script time:")
    timer_all.stop()


# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------


def all_Data():
    #PATH_file_name= f"Data_All_K_{root_PATH.split('/')[-1].split('.root')[0]}.ftr"

    print('Starting')
    files_folder = '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/'

    Files_list_K = sorted([files_folder + x for x in os.listdir(files_folder) if 'ntuple-data18_13TeV_periodK' in x ])

    Files_list = Files_list_K

    """
    additional_filter = "& ((BeeKst_electron0_charge * BeeKst_electron1_charge) <0)"
    additional_filter += "& ((BeeKst_meson0_charge * BeeKst_meson1_charge) < 0)"
    additional_filter += "& (BeeKst_chi2 < 20)"
    additional_filter += "& (BeeKst_diElectron_mass * BeeKst_diElectron_mass <= 6000000)" #bound of low q^2
    additional_filter += "& (1100 <= BeeKst_diElectron_mass * BeeKst_diElectron_mass)" #bound of low q^2
    additional_filter += "& ((BeeKst_isTrueResBd + BeeKst_isTrueNonresBd + BeeKst_isTrueResBdbar + BeeKst_isTrueNonresBdbar) > 0)"
    additional_filter += "& (((4000 < BeeKst_B_mass) & (BeeKst_B_mass < 5700))"
    additional_filter += "& ((4000 < BeeKst_Bbar_mass) & (BeeKst_Bbar_mass < 5700)))"
    """
    

    timer_all = Timer()
    timer_all.start()

    df = pd.DataFrame()
    for root_PATH in Files_list:
        N_entries,process_array = prep_multiprocess(root_PATH=root_PATH, N_process=4,max_entries=None)
        print(f"check array indexes {process_array}")
        timer = Timer()
        timer.start()

        def multi_input(start,end):
            vtx_BeeKst, vtx_GSF, vtx_InDet = root_file_opener(root_PATH, start_entry =start , stop_entry=end)
            if len(vtx_BeeKst)>0:
                data = add_features(data=vtx_BeeKst, data_GSF = vtx_GSF, data_InDet=vtx_InDet)

                # select best 6 Lxy_sig/chi2
                #data=data.sort_values('Lxy_significance_over_B_chi2').groupby(level=0).tail(6).sort_index()
            else:
                data = vtx_BeeKst
            return data

        
        def run_MP():
            mp = Multiprocessor()
            num_proc = 4
            mp.run([
                multi_input,multi_input,multi_input,multi_input],process_array)
            ret = mp.wait() # get all results
            print(f"Shape of each sub-process: Process 1-4: {' '.join([' | ' + str(i.shape)+' | ' for i in ret])}")

            df = pd.DataFrame()
            for i in ret:
                df = pd.concat([df,i],axis=0)
            
            df = df.sort_index()

            return df

        data = run_MP()
        print()
        print(f"{data.shape} is the shape of file {Files_list.index(root_PATH)} out of {len(Files_list)}")
        save_to_feater(data, PATH=f"Data_All_K_{root_PATH.split('/')[-1].split('.root')[0]}.ftr")
        #df = pd.concat([df,data],axis=0)
        print()
        print(f"Accumulated shape: {df.shape} ")        
        print()
        print(f"Time it took to do file {Files_list.index(root_PATH)+1} out of {len(Files_list)} files")
        timer.stop()
        print()
    
    #save_to_feater(df, PATH=PATH_file_name)
    print(f"Total script time:")
    timer_all.stop()


# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------



def all_Signal():
    #PATH_file_name= f"MC_signal_all_{root_PATH.split('/')[-1].split('.root')[0]}.ftr"
    print('Starting')

    Files_list = [
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300590_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300590_part_02.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300590_part_03.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300590_part_04.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300591_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300591_part_02.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300591_part_03.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300592_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300593_part_01.root',
    ]

    
    timer_all = Timer()
    timer_all.start()

    df = pd.DataFrame()
    for root_PATH in Files_list:
        N_entries,process_array = prep_multiprocess(root_PATH=root_PATH, N_process=4,max_entries=None)
        print(f"check array indexes {process_array}")
        timer = Timer()
        timer.start()

        def multi_input(start,end):
            vtx_BeeKst, vtx_GSF, vtx_InDet = root_file_opener(root_PATH, start_entry =start , stop_entry=end)
            if len(vtx_BeeKst)>0:
                data = add_features(data=vtx_BeeKst, data_GSF = vtx_GSF, data_InDet=vtx_InDet)
            else:
                data = vtx_BeeKst
            return data

        
        def run_MP():
            mp = Multiprocessor()
            num_proc = 4
            mp.run([
                multi_input,multi_input,multi_input,multi_input],process_array)
            ret = mp.wait() # get all results
            print(f"Shape of each sub-process: Process 1-4: {' '.join([' | ' + str(i.shape)+' | ' for i in ret])}")

            df = pd.DataFrame()
            for i in ret:
                df = pd.concat([df,i],axis=0)
            
            df = df.sort_index()

            return df

        data = run_MP()
        print()
        print(f"{data.shape} is the shape of file {Files_list.index(root_PATH)} out of {len(Files_list)}")
        #df = pd.concat([df,data],axis=0)
        save_to_feater(data, PATH=f"MC_signal_all_{root_PATH.split('/')[-1].split('.root')[0]}.ftr")
        print()
        print(f"Accumulated shape: {df.shape} ")        
        print()
        print(f"Time it took to do file {Files_list.index(root_PATH)+1} out of {len(Files_list)} files")
        timer.stop()
        print()
    
    #save_to_feater(df, PATH=PATH_file_name)
    print(f"Total script time:")
    timer_all.stop()


# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------



def all_MC_background():
    #PATH_file_name= f"MC_signal_all_{root_PATH.split('/')[-1].split('.root')[0]}.ftr"
    print('Starting')

    Files_list = [
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300718_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300719_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300722_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300723_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300724_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300725_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300726_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300727_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300730_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300731_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300734_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300735_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300738_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300739_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300742_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300743_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300744_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300745_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300748_part_01.root',
        '/groups/hep/kvh318/RK_Star_Analysis/RootData/DataAndMC_v08/ntuple-300749_part_01.root',
        ]

    
    timer_all = Timer()
    timer_all.start()

    df = pd.DataFrame()
    for root_PATH in Files_list:
        N_entries,process_array = prep_multiprocess(root_PATH=root_PATH, N_process=4,max_entries=None)
        print(f"check array indexes {process_array}")
        timer = Timer()
        timer.start()

        def multi_input(start,end):
            vtx_BeeKst, vtx_GSF, vtx_InDet = root_file_opener(root_PATH, start_entry =start , stop_entry=end)
            if len(vtx_BeeKst)>0:
                data = add_features(data=vtx_BeeKst, data_GSF = vtx_GSF, data_InDet=vtx_InDet)
            else:
                data = vtx_BeeKst
            return data

        
        def run_MP():
            mp = Multiprocessor()
            num_proc = 4
            mp.run([
                multi_input,multi_input,multi_input,multi_input],process_array)
            ret = mp.wait() # get all results
            print(f"Shape of each sub-process: Process 1-4: {' '.join([' | ' + str(i.shape)+' | ' for i in ret])}")

            df = pd.DataFrame()
            for i in ret:
                df = pd.concat([df,i],axis=0)
            
            df = df.sort_index()

            return df

        data = run_MP()
        print()
        print(f"{data.shape} is the shape of file {Files_list.index(root_PATH)} out of {len(Files_list)}")
        df = pd.concat([df,data],axis=0)
    save_to_feater(df, PATH=f"MC_background_all_{root_PATH.split('/')[-1].split('.root')[0]}.ftr")
    print()
    print(f"Accumulated shape: {df.shape} ")        
    print()
    print(f"Time it took to do file {Files_list.index(root_PATH)+1} out of {len(Files_list)} files")
    timer.stop()
    print()
    
    #save_to_feater(df, PATH=PATH_file_name)
    print(f"Total script time:")
    timer_all.stop()



if __name__ == "__main__":
    #Signal()
    #Data_SB()
    #Data_SR()
    #all_Data()
    #all_Signal()
    all_MC_background()
