import pandas as pd
import numpy as np
import lightgbm
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.special import logit, expit
import sklearn.metrics
import seaborn as sns

from typing import Optional



class Verstack_train_plots():
    def __init__(self):
        pass

    def plot_optimization_history(self,df_trials:pd.core.frame.DataFrame,fig=None,placement=131):
        ax_hist = fig.add_subplot(placement)

        df_plot = df_trials[df_trials.state == 'COMPLETE']
        direction = 'MINIMIZE' # or 'MAXIMIZE'
        if direction == 'MINIMIZE':
            best_values = np.minimum.accumulate(df_plot.value.values)
        else:
            best_values = np.maximum.accumulate(df_plot.value.values)

        clist= plt.rcParams['axes.prop_cycle'].by_key()['color']
        
        ax_hist.scatter(x=df_plot.number.values,y=df_plot.value.values,color=clist[1],alpha=1,label="Objective Value",)
        ax_hist.plot(df_plot.number.values, best_values, marker="o",color=clist[0],alpha=0.5,label="Best Value",)
        ax_hist.set(title="Optimization History Plot",xlabel="#Trials",ylabel="Objective Value")
        ax_hist.legend()

    def plot_param_imp(self,df_importance:pd.core.frame.DataFrame,fig=None,placement = 132):
        ax_param = fig.add_subplot(placement)

        Importances = df_importance.sort_values('value')
        clist= plt.rcParams['axes.prop_cycle'].by_key()['color']

        ax_param.barh(list(Importances.index), Importances.value.values,fill=False, align="center",edgecolor=clist[0],linewidth=2,tick_label=list(Importances.index))
        for tick in ax_param.yaxis.get_majorticklabels():
            tick.set_horizontalalignment("left")
        ax_param.tick_params(axis="y",direction="out", pad=-10,labelcolor='black',size=5)
        ax_param.set(title="Hyperparameter Importances",ylabel="Hyperparameter",xlabel="Importance for Objective Value")

    def plot_intermediate(self, df_trials:pd.core.frame.DataFrame,fig=None, placement=133):
        ax_inter = fig.add_subplot(placement)

        df_plot = df_trials.sort_values('value')

        clist= plt.rcParams['axes.prop_cycle'].by_key()['color']

        for i in range(1,len(df_plot)-1):
            ax_inter.plot(list(df_plot.iloc[i].intermediate_values.index) ,df_plot.iloc[1].intermediate_values.values,c='black',alpha=0.7)
        ax_inter.plot(list(df_plot.iloc[0].intermediate_values.index),df_plot.iloc[0].intermediate_values.values,c=clist[0],label='Best Trial')
        
        ax_inter.set(title="Intermediate Values Plot",ylabel="Intermediate Value",xlabel="Step")
        ax_inter.legend()

# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------

class plot_response():
    def __init__(self, input_data: pd.core.frame.DataFrame = None, threshold: Optional[float]=None, root_tag:str='BDT1'):
        self.input_data = input_data
        self.root_tag = root_tag

        if (threshold== None) or ((threshold >= 0) and (threshold <= 1)):
            self.threshold = threshold
        else:
            self.threshold = None
            print(f"Threshold/cut were exceeting allowed range which are [0,1], threshold/cut is set to 'None'")

        needed_columns = [f'P_{root_tag}_Bkg', f'P_{root_tag}_Bd', f'P_{root_tag}_BdBar', f'P_{root_tag}_sum',f'P_{root_tag}_max', 'info_truth_matching',  'BeeKst_isTrueNonresBd', 'BeeKst_isTrueNonresBd', 'BeeKst_isTrueResBdbar', 'BeeKst_isTrueNonresBdbar']
        assert needed_columns in list(input_data.columns), f"The following columns are not in the input data:\n{[i for i in needed_columns if i not in list(input_data.columns)]}"
        
    def plot_descision_scores(self,fig:mpl.figure.Figure=None,placemenent:int=131, title:str=None):
        PData = self.input_data
        plot_data = PData.eval(f"P_{self.root_tag}_sum")

        signal_truth_mask = (PData.info_truth_matching == 1)
        data_truth_mask = (PData.info_truth_matching == 0)

        x_range = (logit(PData.eval(f"P_{self.root_tag}_sum.min()")), logit(PData.eval(f"P_{self.root_tag}_sum.max()")))
        bins = 100
        bindwidth = (x_range[1]-x_range[0])/bins
        bindwidth_str = str(float(f"{bindwidth:.1g}")).split('.')[0] if float(str(float(f"{bindwidth:.1g}")).split('.')[-1]) == 0 else str(float(f"{bindwidth:.1g}"))

        ax_scores = fig.add_subplot(placemenent)
        c1,_,_ =ax_scores.hist(logit(plot_data[data_truth_mask]), bins=bins, range=x_range,alpha=0.65,edgecolor="k",label='Real data',density=True,log=True)
        c2,c__2,_ =ax_scores.hist(logit(plot_data[signal_truth_mask]), bins=bins, range=x_range, alpha=0.65,edgecolor="k",label=f"Non-res signal",density=True,log=True)
        if self.threshold != None:
            ax_scores.vlines(logit(self.threshold),0,max(c1+c2),color='black',linestyle=':',label=f"Descision Score Cut: {self.threshold:.3f} | Logit: {logit(self.threshold):.3f}")

        if title != None:
            ax_scores.set_title(title)
        ax_scores.set_ylim(0,max(c1+c2)*100)
        ax_scores.set_xlabel('Probability (Logit Transformed)',loc='right')
        ax_scores.set_ylabel(f"Density / ({bindwidth_str})",loc='top')
        ax_scores.legend(loc = 'upper right', frameon=True)

    def Plot_efficiency_rejection(self,fig:mpl.figure.Figure=None,placemenent:int=131, title:str=None):
        PData = self.input_data
        plot_data = PData.eval(f"P_{self.root_tag}_sum")
        cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']

        signal_truth_mask = (PData.info_truth_matching == 1)
        data_truth_mask = (PData.info_truth_matching == 0)

        hist_eff = []
        hist_rej = []

        lin_space_range = np.linspace(0,1,1001)

        for cut in lin_space_range:
            hist_eff.append(len(plot_data[signal_truth_mask][(plot_data[signal_truth_mask] > cut)]) / len(plot_data[signal_truth_mask]))
            hist_rej.append(1-len(plot_data[data_truth_mask][(plot_data[data_truth_mask] > cut )]) / len(plot_data[data_truth_mask]))

        ax_eff = fig.add_subplot(placemenent)
        ax_eff.plot(lin_space_range,hist_rej,label='Read Data (rejection)',color=cycle[1])
        ax_eff.plot(lin_space_range,hist_eff,label='Non-res Signal (effeciency)',color=cycle[0])

        ax_eff_y= ax_eff.twinx()
        ax_eff_y.set_yticks([]), ax_eff_y.grid(False)

        ax_eff.set_xlabel('Probability',loc='right')
        ax_eff.set_ylabel('Efficiency',loc='top',color=cycle[0])
        ax_eff_y.set_ylabel('Rejection',loc='top',color=cycle[1])
        if title != None:
            ax_eff.set_title(title)
        ax_eff.legend(frameon=True,loc='lower center')

    def plot_roc_curve(self,fig:mpl.figure.Figure=None,placemenent:int=131, title:str=None):
        PData = self.input_data
        plot_data = PData.eval(f"P_{self.root_tag}_sum")
        cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']

        signal_truth_mask = (PData.info_truth_matching == 1)
        data_truth_mask = (PData.info_truth_matching == 0)
        y_truth = PData.info_truth_matching

        ax_roc = fig.add_subplot(placemenent)

        fpr, tpr, _ = sklearn.metrics.roc_curve(y_truth, plot_data)                  # False/True Positive Rate for our model
        auc_score = sklearn.metrics.auc(fpr,tpr) 
        if self.threshold != None:
            tpr_thres=(sum(map(lambda x : x>self.threshold, plot_data[signal_truth_mask])))/len(plot_data[signal_truth_mask])
            fpr_thres=(sum(map(lambda x : x>self.threshold, plot_data[data_truth_mask])))/len(plot_data[data_truth_mask])

        ax_roc.plot(fpr, tpr, label=f'Area Under Curve (AUC) score: {auc_score:5.3f}',color=cycle[0])
        if self.threshold != None:
            ax_roc.scatter(fpr_thres,tpr_thres,s=40, color=cycle[0],label='Cut on ROC curve')

        prec, recall, _ = sklearn.metrics.precision_recall_curve(y_truth, plot_data) 
        if self.threshold != None:
            precision_thres = sklearn.metrics.precision_score(y_truth, plot_data > self.threshold)
            recall__thres = sklearn.metrics.recall_score(y_truth, plot_data > self.threshold)
        AP=sklearn.metrics.average_precision_score(y_truth, plot_data)

        ax_roc.plot(recall, prec, label=f'Average Precision (AP) score: {AP:5.3f}',color=cycle[1])
        if self.threshold != None:
            ax_roc.scatter(recall__thres,precision_thres,s=40, color=cycle[1],label='Cut on Precision-Recall Curve')

        if title != None:
            ax_roc.set_title(title)                  
        ax_roc.set_xlabel('False Postive Rate: '+r'$\frac{\mathrm{False Positive}}{\mathrm{Actual Negative}}$',loc='right',color=cycle[0])
        ax_roc.set_ylabel('True Positive Rate: '+r'$\frac{\mathrm{True Positive}}{\mathrm{True Positive}+}$',loc='top',color=cycle[0])

        ax_roc_x, ax_roc_y=ax_roc.twiny(), ax_roc.twinx()
        ax_roc_x.set_xticks([]), ax_roc_y.set_yticks([])
        ax_roc_x.grid(False),  ax_roc_y.grid(False)
        ax_roc_x.set_xlabel('Recall: '+r'$\frac{\mathrm{True Positive}}{\mathrm{Actual Positive}}$',loc='right',color=cycle[1])
        ax_roc_y.set_ylabel('Precision: '+r'$\frac{\mathrm{True Positive}}{\mathrm{Predicted Positive}}$',loc='top',color=cycle[1])

        ax_roc.legend(frameon=True,loc='lower center')

    def plot_confusionmatrix_3x3(self,fig:mpl.figure.Figure=None,placemenent:int=131, title:str=None):
        if self.threshold != None:
            PData = self.input_data
            plot_data = PData.eval(f"P_{self.root_tag}_sum")
            P_Bd_BdBar=pd.concat([PData.eval(f"P_{self.root_tag}_Bd"),PData.eval(f"P_{self.root_tag}_BdBar")],axis=1)
            cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']

            Bd_truth_mask = (PData.BeeKst_isTrueResBd == 1) | (PData.BeeKst_isTrueNonresBd == 1)
            BdBar_truth_mask = (PData.BeeKst_isTrueResBdbar == 1) | (PData.BeeKst_isTrueNonresBdbar == 1)

            signal_truth_mask = (PData.info_truth_matching == 1)
            data_truth_mask = (PData.info_truth_matching == 0)

            y_predict  = np.array([1 if pred > self.threshold else 0 for pred in plot_data])
            y_predict[(y_predict==1)]=np.argmax(P_Bd_BdBar.values[(y_predict==1)],axis=1)+1
            y_truth = PData.info_truth_matching * ~(Bd_truth_mask | BdBar_truth_mask) + 1 * Bd_truth_mask + 2 * BdBar_truth_mask
                
            ax_conf = fig.add_subplot(placemenent)
            cf_matrix = sklearn.metrics.confusion_matrix(y_truth, y_predict) 
            percen_matrix = sklearn.metrics.confusion_matrix(y_truth, y_predict, normalize='pred') 
            group_counts = ["{0:0.0f}".format(value) for value in cf_matrix.flatten()]
            group_percentages = ["{0:.2%}".format(value) for value in percen_matrix.flatten()]

            #labels = np.asarray([f"{v1}\n{v2}\n{v3}" for v1, v2, v3 in zip(group_names,group_counts,group_percentages)]).reshape(2,2)
            labels = np.asarray([f"{v1}\n{v2}" for v1, v2 in zip(group_counts,group_percentages)]).reshape(3,3)
            ax_conf = sns.heatmap(cf_matrix, annot=labels, fmt='', cmap='Blues',cbar=False)
            ax_conf.set(xlabel='Predicted Values',ylabel='Actual Values')
            ax_conf.xaxis.set_ticklabels(['Real Data','Bd','BdBar'])
            ax_conf.yaxis.set_ticklabels(['Real Data','Bd','BdBar'])
            if title != None:
                ax_conf.set_title(title)
        else:
            print('Threshold/cut is needed for the plotting of a confusion matrix.')

# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------


class TwoD_response_curves():
    def __init__(self, input_data: pd.core.frame.DataFrame = None, threshold: Optional[float]=None, root_tag:str='BDT1'):
        self.input_data = input_data
        self.root_tag = root_tag

        if (threshold== None) or ((threshold >= 0) and (threshold <= 1)):
            self.threshold = threshold
        else:
            self.threshold = None
            print(f"Threshold/cut were exceeting allowed range which are [0,1], threshold/cut is set to 'None'")

        needed_columns = [f'P_{root_tag}_Bkg', f'P_{root_tag}_Bd', f'P_{root_tag}_BdBar', f'P_{root_tag}_sum',f'P_{root_tag}_max', 'info_truth_matching',  'BeeKst_isTrueNonresBd', 'BeeKst_isTrueNonresBd', 'BeeKst_isTrueResBdbar', 'BeeKst_isTrueNonresBdbar']
        assert needed_columns in list(input_data.columns), f"The following columns are not in the input data:\n{[i for i in needed_columns if i not in list(input_data.columns)]}"
        
    def plot_Bd(self,fig:mpl.figure.Figure=None,placemenent:int=121, title:str=None):
        PData = self.input_data
        
        Bd_truth_mask = (PData.BeeKst_isTrueResBd == 1) | (PData.BeeKst_isTrueNonresBd == 1)

        ax_Bd = fig.add_subplot(placemenent)
        h = ax_Bd.hist2d(PData.eval(f"P_{self.root_tag}_Bd")[Bd_truth_mask],PData.eval(f"P_{self.root_tag}_BdBar")[Bd_truth_mask],bins = list(np.linspace(0,1,25)),
                density = True,
                norm = mpl.colors.LogNorm(),
                cmap = plt.cm.Reds)
        fig.colorbar( h[3], ax = ax_Bd)
        ax_Bd.set_xlabel( "Probability to be Bd", loc = "right")
        ax_Bd.set_ylabel( "Probability to be BdBar", loc = "top")
        if title != None:
            ax_Bd.set_title(title)
        else:
            ax_Bd.set_title('MC sample: Bd')

    def plot_BdBar(self,fig:mpl.figure.Figure=None,placemenent:int=122, title:str=None):
        PData = self.input_data
        
        BdBar_truth_mask = (PData.BeeKst_isTrueResBdbar == 1) | (PData.BeeKst_isTrueNonresBdbar == 1)

        ax_BdBar = fig.add_subplot(placemenent)
        h = ax_BdBar.hist2d(PData.eval(f"P_{self.root_tag}_Bd")[BdBar_truth_mask],PData.eval(f"P_{self.root_tag}_BdBar")[BdBar_truth_mask],bins = list(np.linspace(0,1,25)),
                density = True,
                norm = mpl.colors.LogNorm(),
                cmap = plt.cm.Reds)
        fig.colorbar( h[3], ax = ax_BdBar)
        ax_BdBar.set_xlabel( "Probability to be Bd", loc = "right")
        ax_BdBar.set_ylabel( "Probability to be BdBar", loc = "top")
        if title != None:
            ax_BdBar.set_title(title)
        else:
            ax_BdBar.set_title('MC sample: BdBar')


""" ------------------------------------------------------------
            For Fitting
------------------------------------------------------------ """
