import sklearn.inspection
import random
import pandas as pd
import numpy as np
random.seed(42)
import toml
import os
import sys
sys.path.append("..")
from utils import Utils
import datetime as dt


config = toml.load('config.toml')
Storage = Utils.HDF5_IO(config['PATHS']['Storage'])

#################################################################################

List_of_scorer = {
    'AUC': sklearn.metrics.make_scorer(sklearn.metrics.roc_auc_score,greater_is_better=True),
    'RMSE': sklearn.metrics.make_scorer(sklearn.metrics.mean_squared_error,squared=False,greater_is_better=False),
}

#################################################################################

class feature_importance:
    def __init__(self,X_data:pd.core.frame.DataFrame,y_data:pd.core.frame.DataFrame, root_tag:str,label:str):
        self.X_data = X_data.copy()
        self.y_data = y_data.copy()
        self.label = label
        self.root_tag = root_tag
        self.lgbm_model = None
        self.features = None
        self.load_lightgbm_model_and_set_features()
        self.rets = pd.DataFrame(index = self.features)

    def load_lightgbm_model_and_set_features(self) -> None:
        self.lgbm_model = Storage.load_lgbm(key=f"{self.root_tag}/model")
        self.features = list(self.lgbm_model.feature_name())

    def SHAP(self) -> None:
        print(f'Init SHAP importances ({self.label})...')

        X_scaled = Utils.pd_scale(Storage,self.X_data.copy()[self.features], root_tag= self.root_tag)
        lgbm_shap = self.lgbm_model.predict(X_scaled.reindex(columns=self.features), pred_contrib=True)
        df = pd.DataFrame(columns=['SHAP_'+self.label],index= self.features, data= np.abs(lgbm_shap[:,:-1]).mean(0))
        self.rets = pd.concat([self.rets,df],axis=1)
        print('Done with SHAP importances')
    
    def Native_lgbm(self) -> None:
        print(f'Init Native LGBM importances ({self.label})...')
        
        native_importance = self.lgbm_model.feature_importance()
        native_indices = np.argsort(native_importance)
        Native_importance = pd.DataFrame({'features': np.array(self.features)[native_indices][::-1], 'native_values': native_importance[native_indices][::-1]})
        
        df = pd.DataFrame(columns=['Native_'+self.label],index= self.features, data= native_importance)
        self.rets = pd.concat([self.rets,df],axis=1)
        print('Done with Native LGBM importances')

    def Permutation(self, scorer:sklearn.metrics._scorer._PredictScorer, N_repeats:int) -> None:
        print(f'Init Permutation importances ({self.label})...')

        PERM_score = sklearn.inspection.permutation_importance(self.lgbm_model, self.X_data.copy()[self.features], self.y_data.copy(), n_repeats=N_repeats, random_state=42,n_jobs=1,scoring=scorer)
        df = pd.DataFrame(columns=['perm_'+self.label],index= self.features, data= PERM_score.importances_mean)
        self.rets = pd.concat([self.rets,df],axis=1)
        print('Done with Permutation importances.')

    def get_Frame(self) -> pd.core.frame.DataFrame:
        return self.rets

#################################################################################

if __name__ == "__main__":
    A = dt.datetime.now()
    feature_config = pd.read_pickle('feature_config.pkl')
    XData = pd.read_pickle('feature_importance_X_data__.pkl')
    yData = pd.read_pickle('feature_importance_y_data__.pkl')
    os.remove('feature_config.pkl')
    os.remove("feature_importance_X_data__.pkl")
    os.remove("feature_importance_y_data__.pkl")

    get_importance = feature_importance(X_data = XData, y_data = yData, root_tag = feature_config.root_tag[0], label = feature_config.label[0])
    get_importance.Native_lgbm()
    get_importance.SHAP()
    RMSE_score = sklearn.metrics.make_scorer(sklearn.metrics.mean_squared_error,squared=False,greater_is_better=False)
    get_importance.Permutation(scorer=List_of_scorer['RMSE'],N_repeats=10)
    get_importance.get_Frame().to_pickle(f"{feature_config.root_tag[0]+'_'+feature_config.label[0]}1.pkl")


    print(dt.datetime.now()-A)