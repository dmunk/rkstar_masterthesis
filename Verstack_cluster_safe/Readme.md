# A modified Verstack LGBMTuner 

This are a modied version of the python library: Verstack developed by: Danil Zherebtsov

The Git repo at url: https://github.com/DanilZherebtsov/verstack

Modification are made by:
Daniel Hans Munk, danielhansmunk@gmail.com

The modification inlude the option of setting the parameter: n_jobs. Another new feature added is that logging to file: log.log when running the LGBMTuner 

The current version of LGBMTuner is not cluster safe since if more people are using the cluster it will take all cores minus two. This is not good when multiple people tries to use the cluster.

The change is such that if n_jobs is not set to anything it has a default value of "None" and LGBMTuner will behave a normal and greedily use all CPU's.
if n_jobs = A, then A will be the number (integer) the LGBMTuner will use.